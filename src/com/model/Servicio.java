package com.model;

import com.db.DBTabla;
import com.db.DataBaseController;
import com.tool.Listable;
import com.tool.PrinterDate;
import com.tool.PrinterList;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class Servicio implements Listable {
	private ObjectProperty<Integer> id;
	private ObjectProperty<PrinterDate> fecha;
	private ObjectProperty<Cliente> cliente;
	private ObjectProperty<Proyecto> proyecto;

	private SimpleStringProperty responsable;
	private SimpleStringProperty vehiculo;
	private SimpleStringProperty observaciones;

	private ObjectProperty<PrinterList<Config>> equipos;
	private ObjectProperty<PrinterList<Empleado>> empleado;
	private ObjectProperty<PrinterList<String>> planos;
	private ObjectProperty<PrinterList<Insumo>> materiales;

	public Servicio(Integer id) {
		super();
		this.id = new SimpleObjectProperty<Integer>(id);
		this.fecha = new SimpleObjectProperty<PrinterDate>(PrinterDate.today());
		this.cliente = new SimpleObjectProperty<Cliente>();
		this.proyecto = new SimpleObjectProperty<Proyecto>();

		this.responsable = new SimpleStringProperty("-");
		this.vehiculo = new SimpleStringProperty("-");
		this.observaciones = new SimpleStringProperty("-");

		this.equipos = new SimpleObjectProperty<PrinterList<Config>>(new PrinterList<Config>());
		this.empleado = new SimpleObjectProperty<PrinterList<Empleado>>(new PrinterList<Empleado>());
		this.planos = new SimpleObjectProperty<PrinterList<String>>(new PrinterList<String>());
		this.materiales = new SimpleObjectProperty<PrinterList<Insumo>>(new PrinterList<Insumo>());

		this.equipos.get().setListClass(Config.class);
		this.empleado.get().setListClass(Empleado.class);
		this.planos.get().setListClass(String.class);
		this.materiales.get().setListClass(Insumo.class);

	}

	public ObjectProperty<Integer> idProperty() {
		return id;
	}

	public ObjectProperty<PrinterDate> fechaProperty() {
		return fecha;
	}

	public ObjectProperty<Cliente> clienteProperty() {
		return cliente;
	}

	public ObjectProperty<Proyecto> proyectoProperty() {
		return proyecto;
	}

	public SimpleStringProperty responsableProperty() {
		return responsable;
	}

	public SimpleStringProperty vehiculoProperty() {
		return vehiculo;
	}

	public SimpleStringProperty observacionesProperty() {
		return observaciones;
	}

	public ObjectProperty<PrinterList<Empleado>> empleadoProperty() {
		return empleado;
	}

	public ObjectProperty<PrinterList<String>> planosProperty() {
		return planos;
	}

	public ObjectProperty<PrinterList<Insumo>> materialesProperty() {
		return materiales;
	}

	public ObjectProperty<PrinterList<Config>> equiposProperty() {
		return equipos;
	}

	@Override
	public String getStringId() {
		return getId() + " | " + fechaProperty().get();
	}

	@Override
	public Integer getId() {
		return idProperty().get();
	}

	@Override
	public void setId(Integer id) {
		if (this.idProperty().get() == null) {
			this.idProperty().setValue(id);
		}

	}

	@Override
	public DBTabla<? extends Listable> getTabla() {
		return (DBTabla<Servicio>) DataBaseController.instance().getTabla(entidadesEnum.SERVICIOS.getTabla());
	}

	@Override
	public void salirLista() {
		// TODO Auto-generated method stub
		
	}

}
