package com.model;

public enum entidadesEnum {
	CLIENTES("Clientes"), PRODUCTOS("Productos"), SERVICIOS("Servicios"), EMPLEADOS("Empleados"),
	PROYECTOS("Proyectos"), CONFIGURACION("Config"), ENTRADAS("Entradas");

	private String nombre;

	entidadesEnum(String nombre) {
		this.nombre = nombre;
	}

	public String getTabla() {
		return nombre;
	}

	@Override
	public String toString() {
		return nombre;
	}

	public String getTabName() {
		return nombre;
	}

	public String getFxml() {
		return nombre;
	}
}
