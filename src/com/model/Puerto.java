package com.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class Puerto {

	public static final int size = 3;
//	private ObjectProperty<Integer> id;
	private SimpleStringProperty nombre;
	private ObjectProperty<Integer> entrada;
	private SimpleStringProperty salida;
	
	

	public Puerto() {
//		this.id = new SimpleObjectProperty<Integer>(puerto);
		this.nombre = new SimpleStringProperty(null);
		this.entrada = new SimpleObjectProperty<Integer>(null);
		this.salida = new SimpleStringProperty(null);
		}

//	public ObjectProperty<Integer> getId() {
//		return id;
//	}

	public SimpleStringProperty getNombre() {
		return nombre;
	}

	public ObjectProperty<Integer> getEntrada() {
		return entrada;
	}

	public SimpleStringProperty getSalida() {
		return salida;
	}
	@Override
	public String toString() {
		return entrada.get() + ":"+ nombre.get();
	}

		public static Collection<Puerto> parse(List<String[]> registro) {
		List<Puerto> lista = new ArrayList<Puerto>();
		registro.forEach(r->{
			Puerto puerto = new Puerto();
			puerto.nombre.set(r[0]);
			puerto.entrada.set(Integer.parseInt(r[1]));
			puerto.salida.set(r[2]);
			
			lista.add(puerto);
		});
		return lista;
	}
}
