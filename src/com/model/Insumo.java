package com.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Insumo {
	public static final int size = 2;
	private int cantidad;
	private String detalle;
	public Insumo(int cantidad, String detalle) {
		super();
		this.cantidad = cantidad;
		this.detalle = detalle;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	
	public static Collection<Insumo> parse(List<String[]> registro) {
		List<Insumo> lista = new ArrayList<Insumo>();
		registro.forEach(r->{
			Insumo ip = new Insumo(Integer.parseInt(r[0]),r[1]);
			lista.add(ip);
		});
		return lista;
	}
	
	
}
