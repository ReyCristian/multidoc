package com.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.tool.Listable;
import com.tool.PrinterList;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public abstract class Persona implements Listable {

	private static Map<Integer, Persona> listaCreados = new HashMap<Integer, Persona>();
	private ObjectProperty<Integer> id;
	private SimpleStringProperty nombre;

	private ObjectProperty<PrinterList<Integer>> telefono;
	private ObjectProperty<PrinterList<String>> email;

	public Persona(Integer id) {
		this.id = new SimpleObjectProperty<Integer>(null);
		this.nombre = new SimpleStringProperty("-");
		this.telefono = new SimpleObjectProperty<PrinterList<Integer>>(new PrinterList<Integer>());
		this.email = new SimpleObjectProperty<PrinterList<String>>(new PrinterList<String>());

		telefono.get().setListClass(Integer.class);
		email.get().setListClass(String.class);

		if (id != null)
			this.setId(id);
	}

//	private void addLista(Persona persona) {
//		listaCreados.put(persona.getId(), persona);
//	}

	static public Persona get(Integer id) {
		Persona persona = listaCreados.get(id);
		return persona;
	}

	public ObjectProperty<Integer> idProperty() {
		return id;
	}

	public SimpleStringProperty nombreProperty() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre.set(nombre);
	}

	public String getNombre() {
		return nombre.get();
	}

	public void addTelefono(int telefono) {
		this.telefono.get().add(telefono);
	}

	public ObjectProperty<PrinterList<Integer>> telefonoProperty() {
		return telefono;
	}

	public ObjectProperty<PrinterList<String>> emailProperty() {
		return email;
	}

	@Override
	public String toString() {
		return getNombre().toString();
	}

	public static Collection<Persona> getLista() {
		return listaCreados.values();
	}

	@Override
	public String getStringId() {
		return getId() + " | " + getNombre();
	}

	@Override
	abstract public void setId(Integer id);

	public Integer getId() {
		return id.get();
	}

	@Override
	public void salirLista() {
		getLista().remove(this);

	}
}
