package com.model;

import com.db.DBTabla;
import com.tool.Listable;
import com.tool.PrinterList;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class Usuario extends Persona {

	private SimpleStringProperty nombreUsuario;
	private SimpleStringProperty pass;
	private SimpleStringProperty sector;
	private SimpleStringProperty puesto;

	private SimpleObjectProperty<PrinterList<String>> permisos;

	public Usuario(int i) {
		super(i);
		this.nombreUsuario = new SimpleStringProperty("-");
		this.pass = new SimpleStringProperty("1234");

		this.sector = new SimpleStringProperty("-");
		this.puesto = new SimpleStringProperty("-");

		this.permisos = new SimpleObjectProperty<PrinterList<String>>(new PrinterList<String>());

		this.permisos.get().setListClass(String.class);
	}

	public SimpleStringProperty getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(SimpleStringProperty nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public SimpleStringProperty getPass() {
		return pass;
	}

	public void setPass(SimpleStringProperty pass) {
		this.pass = pass;
	}

	public SimpleStringProperty getSector() {
		return sector;
	}

	public void setSector(SimpleStringProperty sector) {
		this.sector = sector;
	}

	public SimpleStringProperty getPuesto() {
		return puesto;
	}

	public void setPuesto(SimpleStringProperty puesto) {
		this.puesto = puesto;
	}

	public SimpleObjectProperty<PrinterList<String>> getPermisos() {
		return permisos;
	}

	public void setPermisos(SimpleObjectProperty<PrinterList<String>> permisos) {
		this.permisos = permisos;
	}

	@Override
	public String toString() {
		return nombreUsuario.get().toString();
	}

	@Override
	public DBTabla<? extends Listable> getTabla() {
		return null;
		}

	@Override
	public void setId(Integer id) {
		this.idProperty().set(id);
	}


	


}
