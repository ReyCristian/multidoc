package com.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class IP {
	
	public static final int size = 6;
//	private ObjectProperty<Integer> id;
	private SimpleStringProperty ip;
	private ObjectProperty<Integer> mask;
	private SimpleStringProperty gate;
	private SimpleStringProperty mac;
	private SimpleStringProperty dns1;
	private SimpleStringProperty dns2;
	public IP() {
		super();
//		this.id = new SimpleObjectProperty<Integer>(id);
		this.ip = new SimpleStringProperty("-");
		this.mask = new SimpleObjectProperty<Integer>(24);
		this.gate = new SimpleStringProperty("-");
		this.mac = new SimpleStringProperty("-");
		this.dns1 = new SimpleStringProperty("-");
		this.dns2 = new SimpleStringProperty("-");
		
	}
//	public ObjectProperty<Integer> getId() {
//		return id;
//	}
	public SimpleStringProperty getIp() {
		return ip;
	}
	
	public ObjectProperty<Integer> getMask() {
		return mask;
	}
	public SimpleStringProperty getGate() {
		return gate;
	}
	public SimpleStringProperty getMac() {
		return mac;
	}
	public SimpleStringProperty getDns1() {
		return dns1;
	}
	public SimpleStringProperty getDns2() {
		return dns2;
	}


	public static Collection<IP> parse(List<String[]> registro) {
		List<IP> lista = new ArrayList<IP>();
		registro.forEach(r->{
			IP ip = new IP();
			ip.ip.set(r[0]);
			ip.mask.set(Integer.parseInt(r[1]));
			ip.gate.set(r[2]);
			ip.mac.set(r[3]);
			ip.dns1.set(r[4]);
			ip.dns2.set(r[5]);
			
			
			lista.add(ip);
		});
		return lista;
	}
	
	
	
	
}
