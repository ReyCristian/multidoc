package com.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.db.DBTabla;
import com.db.DataBaseController;
import com.tool.Listable;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class Empleado extends Persona {

	private SimpleStringProperty apellido;
	private ObjectProperty<Integer> DNI;
	private static Map<Integer, Persona> listaCreados = new HashMap<Integer, Persona>();

	public Empleado(Integer id) {
		super(null);
		this.apellido = new SimpleStringProperty("-");
		this.DNI = new SimpleObjectProperty<Integer>(null);

		if (id != null)
			this.setId(id);

	}

	public SimpleStringProperty apellidoProperty() {
		return apellido;
	}

	public void setDireccion(String apellido) {
		this.apellido.set(apellido);
	}

	public String getDireccion() {
		return apellido.get();
	}

	public void setDNI(int DNI) {
		this.DNI.set(DNI);
	}

	public ObjectProperty<Integer> DNIProperty() {
		return DNI;
	}

	public int getDNI() {
		return DNI.get();
	}

	public static Collection<Persona> getLista() {
		return listaCreados.values();
	}

	protected void addLista(Persona persona) {
		listaCreados.put(persona.getId(), persona);
	}

	static public Empleado get(Integer id) {
		Empleado empleado = (Empleado) listaCreados.get(id);
		if (empleado == null) {
			List<Empleado> empleados = (List<Empleado>) DataBaseController.instance().get("Empleados", id);
			if (empleados.size() > 0)
				empleado = empleados.get(0);
		}

		return empleado;
	}

	@Override
	public DBTabla<? extends Listable> getTabla() {
		return (DBTabla<Empleado>) DataBaseController.instance().getTabla(entidadesEnum.EMPLEADOS.getTabla());
	}
	@Override
	public void setId(Integer id) {
		if (this.getId() == null) {
			this.idProperty().set(id);
			addLista(this);
		}

	}

	@Override
	public void salirLista() {
		getLista().remove(this);

	}
}
