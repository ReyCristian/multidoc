package com.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class Solucion {

	public static final int size = 3;

	private SimpleObjectProperty<Integer> id;

	private SimpleStringProperty solucion;
	private SimpleStringProperty comentario;




	public Solucion(Integer s) {
		super();
		this.id = new SimpleObjectProperty<Integer>(null);
		this.solucion = new SimpleStringProperty("-");
		this.comentario = new SimpleStringProperty("-");
		}

	public SimpleStringProperty get() {
		return solucion;
	}
	
	
	public void set(String problema) {
		this.solucion.set(problema);
	}
	
	public SimpleObjectProperty<Integer> idProperty() {
		return id;
	}
	
	
	@Override
	public String toString() {
		return solucion.get().toString();
	}

	public SimpleStringProperty getComentario() {
		return comentario;
	}

	public static Collection<? extends Solucion> parse(List<String[]> registro) {
		List<Solucion> lista = new ArrayList<Solucion>();
		registro.forEach(s -> {
			Solucion solucion = new Solucion(Integer.parseInt(s[0]));
			solucion.solucion.set(s[1]);
			solucion.comentario.set(s[1]);
			lista.add(solucion);
		});
		return lista;
	}

	

	
}
