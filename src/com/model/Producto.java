package com.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.db.DBTabla;
import com.db.DataBaseController;
import com.tool.Listable;
import com.tool.PrinterList;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Producto implements Listable {

	private ObjectProperty<Integer> prodId;
	private StringProperty modelo;
	private StringProperty tipoProducto;
	private StringProperty marca;
	private StringProperty caracteristica;
	private SimpleObjectProperty<PrinterList<String>> manuales;
	private SimpleObjectProperty<PrinterList<String>> observaciones;
	private SimpleObjectProperty<PrinterList<Problema>> problemas;

	private static Map<Integer, Producto> listaCreados = new HashMap<Integer, Producto>();

	public Producto(Integer prod_ID) {
		this(prod_ID, "");

	}

	public Producto(Integer prod_ID, String modelo) {
		this.prodId = new SimpleObjectProperty<Integer>(null);
		this.modelo = new SimpleStringProperty(modelo);
		this.tipoProducto = new SimpleStringProperty("");
		this.marca = new SimpleStringProperty("");
		this.caracteristica = new SimpleStringProperty("");
		this.manuales = new SimpleObjectProperty<PrinterList<String>>(new PrinterList<String>());
		this.observaciones = new SimpleObjectProperty<PrinterList<String>>(new PrinterList<String>());
		this.problemas = new SimpleObjectProperty<PrinterList<Problema>>(new PrinterList<Problema>());

		this.manuales.get().setListClass(String.class);
		this.observaciones.get().setListClass(String.class);
		this.problemas.get().setListClass(Problema.class);

		if (prod_ID != null)
			this.setId(prod_ID);
	}

	public void setId(Integer prod_ID) {
		if (this.prodId.get() == null) {
			this.prodId.setValue(prod_ID);
			addLista(this);
		}

	}

	public StringProperty modeloProperty() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo.set(modelo);
	}

	public StringProperty tipoProductoProperty() {
		return tipoProducto;
	}

	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto.set(tipoProducto);
		;
	}

	public StringProperty marcaProperty() {
		return marca;
	}

	public void setMarca(StringProperty marca) {
		this.marca = marca;
	}

	public StringProperty caracteristicaProperty() {
		return caracteristica;
	}

	public void setCaracteristica(StringProperty caracteristica) {
		this.caracteristica = caracteristica;
	}

	public ObjectProperty<Integer> prodIdProperty() {
		return prodId;
	}

	public Integer getId() {
		return prodId.get();
	}

	public String getTipoProducto() {
		return tipoProducto.get();
	}

	public String getMarca() {
		return marca.get();
	}

	public String getCaracteristica() {
		return caracteristica.get();
	}

	public String getModelo() {
		return modelo.get();
	}

	public SimpleObjectProperty<PrinterList<String>> manualesProperty() {
		return manuales;
	}

	public SimpleObjectProperty<PrinterList<String>> observacionesProperty() {
		return observaciones;
	}

	public SimpleObjectProperty<PrinterList<Problema>> problemasProperty() {
		return problemas;
	}

	@Override
	public String getStringId() {
		return getId() + " | " + getModelo();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getModelo().toString();
	}

	public static Producto get(Integer id) {
		Producto producto = listaCreados.get(id);
		if (producto == null) {
			producto = (Producto) DataBaseController.instance().get("Productos", id).get(0);
		}

		return producto;
	}

	public static Collection<Producto> getLista() {
		return listaCreados.values();
	}

	private void addLista(Producto prod) {
		listaCreados.put(prod.getId(), prod);
	}

	@Override
	public DBTabla<? extends Listable> getTabla() {
		return (DBTabla<Producto>) DataBaseController.instance().getTabla(entidadesEnum.PRODUCTOS.getTabla());
	}

	@Override
	public void salirLista() {
		getLista().remove(this);

	}
}
