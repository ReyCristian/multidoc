package com.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.db.DBTabla;
import com.db.DataBaseController;
import com.tool.Listable;
import com.tool.PrinterList;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class Config implements Listable {
	private ObjectProperty<Integer> id;
	private SimpleStringProperty serial;
	private ObjectProperty<Producto> producto;
	private SimpleStringProperty nombre;
	private SimpleStringProperty version;
	private SimpleStringProperty modo;
	private SimpleStringProperty observaciones;
	private SimpleObjectProperty<PrinterList<Problema>> problemas;
	private SimpleObjectProperty<PrinterList<String>> impresiones;
	private SimpleObjectProperty<PrinterList<String>> respaldos;
	private SimpleObjectProperty<PrinterList<IP>> ip;
	private SimpleObjectProperty<PrinterList<Funcion>> funciones;
	private SimpleObjectProperty<PrinterList<Puerto>> puertos;
	private SimpleObjectProperty<PrinterList<Conexion>> conexiones;
	private SimpleObjectProperty<PrinterList<Usuario>> usuarios;

	private static Map<Integer, Config> listaCreados = new HashMap<Integer, Config>();

	public Config(Integer id) {
		super();
		this.id = new SimpleObjectProperty<Integer>(null);
		this.serial = new SimpleStringProperty(null);
		this.nombre = new SimpleStringProperty(null);
		this.version = new SimpleStringProperty(null);
		this.modo = new SimpleStringProperty(null);
		this.observaciones = new SimpleStringProperty(null);
		this.producto = new SimpleObjectProperty<Producto>(null);
		this.problemas = new SimpleObjectProperty<PrinterList<Problema>>(new PrinterList<Problema>());
		this.impresiones = new SimpleObjectProperty<PrinterList<String>>(new PrinterList<String>());
		this.respaldos = new SimpleObjectProperty<PrinterList<String>>(new PrinterList<String>());
		this.ip = new SimpleObjectProperty<PrinterList<IP>>(new PrinterList<IP>());
		this.funciones = new SimpleObjectProperty<PrinterList<Funcion>>(new PrinterList<Funcion>());
		this.puertos = new SimpleObjectProperty<PrinterList<Puerto>>(new PrinterList<Puerto>());
		this.conexiones = new SimpleObjectProperty<PrinterList<Conexion>>(new PrinterList<Conexion>());
		this.usuarios = new SimpleObjectProperty<PrinterList<Usuario>>(new PrinterList<Usuario>());

		this.problemas.get().setListClass(Problema.class);
		this.impresiones.get().setListClass(String.class);
		this.respaldos.get().setListClass(String.class);
		this.ip.get().setListClass(IP.class);
		this.funciones.get().setListClass(Funcion.class);
		this.puertos.get().setListClass(Puerto.class);
		this.conexiones.get().setListClass(Conexion.class);
		this.usuarios.get().setListClass(Usuario.class);

		if (id != null) {
			setId(id);
		}
	}

	private void addLista(Config conf) {
		listaCreados.put(conf.idProperty().get(), conf);
	}

	public ObjectProperty<Integer> idProperty() {
		return id;
	}

	public SimpleStringProperty serialProperty() {
		return serial;
	}

	public ObjectProperty<Producto> productoProperty() {
		return producto;
	}

	public SimpleStringProperty nombreProperty() {
		return nombre;
	}

	public SimpleStringProperty versionProperty() {
		return version;
	}

	public SimpleStringProperty modoProperty() {
		return modo;
	}

	public SimpleStringProperty observacionesProperty() {
		return observaciones;
	}

	public SimpleObjectProperty<PrinterList<Problema>> problemasProperty() {
		return problemas;
	}

	public SimpleObjectProperty<PrinterList<String>> impresionesProperty() {
		return impresiones;
	}

	public SimpleObjectProperty<PrinterList<String>> respaldosProperty() {
		return respaldos;
	}

	public SimpleObjectProperty<PrinterList<IP>> ipProperty() {
		return ip;
	}

	public SimpleObjectProperty<PrinterList<Funcion>> funcionesProperty() {
		return funciones;
	}

	public SimpleObjectProperty<PrinterList<Puerto>> puertosProperty() {
		return puertos;
	}

	public SimpleObjectProperty<PrinterList<Conexion>> conexionesProperty() {
		return conexiones;
	}

	@Override
	public String toString() {
		return getStringId();
	}

	public static Collection<Config> getLista() {
		return listaCreados.values();
	}

	@Override
	public String getStringId() {
		return idProperty().get() + " | " + nombreProperty().get();
	}

	public static Config get(Integer id) {
		Config config = listaCreados.get(id);
		if (config == null) {
			List<Config> configuraciones = (List<Config>) DataBaseController.instance().get("Config", id);
			if (configuraciones.size() > 0)
				config = configuraciones.get(0);
		}

		return config;
	}

	public static Collection<? extends Config> parse(List<String> registro) {
		List<Config> lista = new ArrayList<Config>();
		registro.forEach(equipo -> {
			lista.add(Config.get(Integer.parseInt(equipo)));
		});
		return lista;
	}

	public SimpleObjectProperty<PrinterList<Usuario>> usuariosProperty() {
		return usuarios;
	}

	@Override
	public Integer getId() {
		return idProperty().get();
	}

	@Override
	public void setId(Integer id) {
		if (this.getId() == null) {
			this.idProperty().set(id);
			addLista(this);
		}
	}

	@Override
	public DBTabla<? extends Listable> getTabla() {
		return (DBTabla<Config>) DataBaseController.instance().getTabla(entidadesEnum.CONFIGURACION.getTabla());
	}

	@Override
	public void salirLista() {
		getLista().remove(this);

	}
}
