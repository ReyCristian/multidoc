package com.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.db.DBTabla;
import com.db.DataBaseController;
import com.db.TablaClientes;
import com.tool.Listable;

import javafx.beans.property.SimpleStringProperty;

public class Cliente extends Persona {

	private SimpleStringProperty direccion;
	private static Map<Integer, Persona> listaCreados = new HashMap<Integer, Persona>();

	public Cliente(Integer id) {
		super(null);
		this.direccion = new SimpleStringProperty("-");
		if (id != null)
			this.setId(id);
	}

	private void addLista(Persona persona) {
		listaCreados.put(persona.getId(), persona);
	}

	static public Cliente get(Integer id) {
		Cliente cliente = (Cliente) listaCreados.get(id);
		if (cliente == null) {
			cliente = (Cliente) DataBaseController.instance().get("Clientes", id).get(0);
		}
		return cliente;
	}

	public SimpleStringProperty direccionProperty() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion.set(direccion);
	}

	public String getDireccion() {
		return direccion.get();
	}

	public static Collection<Persona> getLista() {
		return listaCreados.values();
	}

	@Override
	public DBTabla<? extends Listable> getTabla() {
		return (DBTabla<Cliente>) DataBaseController.instance().getTabla(entidadesEnum.CLIENTES.getTabla());
	}

	@Override
	public void setId(Integer id) {
		if (this.getId() == null) {
			this.idProperty().set(id);
			addLista(this);
		}

	}

	@Override
	public void salirLista() {
		getLista().remove(this);

	}
}
