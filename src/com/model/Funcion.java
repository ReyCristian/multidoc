package com.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class Funcion {
	final public static int size=2;
	private SimpleStringProperty atajo;
	private SimpleStringProperty funcion;
	
	public Funcion() {
		super();
		this.atajo = new SimpleStringProperty(null);
		this.funcion = new SimpleStringProperty(null);
	}

	public SimpleStringProperty getAtajo() {
		return atajo;
	}

	public SimpleStringProperty getFuncion() {
		return funcion;
	}

	public static Collection<Funcion> parse(List<String[]> registro) {
		List<Funcion> lista = new ArrayList<Funcion>();
		registro.forEach(r->{
			Funcion f = new Funcion();
			f.atajo.set(r[0]);
			f.funcion.set(r[1]);
			lista.add(f);
		});
		return lista;
	}
	
	
	
	
	
}
