package com.model;

import com.tool.PrinterList;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class Problema {

	private SimpleObjectProperty<Integer> id;

	private SimpleStringProperty problema;
	
	private int idProd=0;

	private SimpleObjectProperty<PrinterList<Solucion>> soluciones;



	public Problema(Integer id) {
		super();
		this.id = new SimpleObjectProperty<Integer>(id);
		this.problema = new SimpleStringProperty("-");
		this.soluciones = new SimpleObjectProperty<PrinterList<Solucion>>(new PrinterList<Solucion>());
	
		this.soluciones.get().setListClass(Solucion.class);
	}

	public SimpleStringProperty get() {
		return problema;
	}
	
	
	public void set(String problema) {
		this.problema.set(problema);
	}
	
	public SimpleObjectProperty<Integer> idProperty() {
		return id;
	}
	
	public PrinterList<Solucion> getSoluciones() {
		return soluciones.get();
	}
	
	@Override
	public String toString() {
		return problema.get().toString();
	}

	public SimpleObjectProperty<PrinterList<Solucion>> solucionesProperty() {
		return soluciones;
	}

	public int getIdProd() {
		return idProd;
	}

	public void setIdProd(int idProd) {
		this.idProd = idProd;
	}

	

	
}
