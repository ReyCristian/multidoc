package com.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class Conexion{

//	private ObjectProperty<Integer> id;
	private ObjectProperty<Config> confP_ID;
	private SimpleStringProperty tipo;

	public Conexion() {
		super();
//		this.id = new SimpleObjectProperty<Integer>(id);
		this.confP_ID = new SimpleObjectProperty<Config>();
		this.tipo = new SimpleStringProperty();
	}

//	public ObjectProperty<Integer> idProperty() {
//		return id;
//	}


	public ObjectProperty<Config> confP_IDProperty() {
		return confP_ID;
	}

	public SimpleStringProperty tipoProperty() {
		return tipo;
	}

	public static Collection<? extends Conexion> parse(List<String[]> registro) {
		List<Conexion> lista = new ArrayList<Conexion>();
		registro.forEach(e -> {
			Conexion conexion = new Conexion();
			conexion.confP_ID.set(Config.get(Integer.parseInt(e[0])));
			conexion.tipo.set(e[1]);
			lista.add(conexion);
		});
		return lista;
	}



}
