package com.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.db.DBTabla;
import com.db.DataBaseController;
import com.tool.Listable;
import com.tool.PrinterList;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class Proyecto implements Listable {
	private static Map<Integer, Proyecto> listaCreados = new HashMap<Integer, Proyecto>();
	private ObjectProperty<Integer> id;
	private SimpleStringProperty nombre;
	private ObjectProperty<Cliente> cliente;
	private ObjectProperty<PrinterList<String>> observaciones;
	private ObjectProperty<PrinterList<String>> planos;
	private ObjectProperty<PrinterList<String>> informes;

	public Proyecto(Integer id) {
		super();
		this.id = new SimpleObjectProperty<Integer>(id);
		this.nombre = new SimpleStringProperty("-");
		this.cliente = new SimpleObjectProperty<Cliente>();
		this.observaciones = new SimpleObjectProperty<PrinterList<String>>(new PrinterList<String>());
		this.planos = new SimpleObjectProperty<PrinterList<String>>(new PrinterList<String>());
		this.informes = new SimpleObjectProperty<PrinterList<String>>(new PrinterList<String>());

		this.observaciones.get().setListClass(String.class);
		this.planos.get().setListClass(String.class);
		this.informes.get().setListClass(String.class);

		if (id != null)
			setId(id);

	}

	public ObjectProperty<Integer> idProperty() {
		return id;
	}

	public SimpleStringProperty nombreProperty() {
		return nombre;
	}

	public ObjectProperty<Cliente> clienteProperty() {
		return cliente;
	}

	public ObjectProperty<PrinterList<String>> observacionesProperty() {
		return observaciones;
	}

	public ObjectProperty<PrinterList<String>> planosProperty() {
		return planos;
	}

	public ObjectProperty<PrinterList<String>> informesProperty() {
		return informes;
	}

	public static Collection<Proyecto> getLista() {
		return listaCreados.values();
	}

	@Override
	public String getStringId() {
		return idProperty().get() + " | " + nombreProperty().get();
	}

	public static Proyecto get(Integer id) {
		Proyecto proyecto = (Proyecto) listaCreados.get(id);
		if (proyecto == null) {
			proyecto = (Proyecto) DataBaseController.instance().get("Proyectos", id).get(0);
		}
		return proyecto;
	}

	private void addLista(Proyecto proyecto) {
		listaCreados.put(proyecto.idProperty().get(), proyecto);
	}

	@Override
	public Integer getId() {
		return idProperty().get();
	}

	@Override
	public void setId(Integer id) {
		if (this.getId() == null) {
			this.idProperty().set(id);
			addLista(this);
		}

	}

	@Override
	public DBTabla<? extends Listable> getTabla() {
		return (DBTabla<Proyecto>) DataBaseController.instance().getTabla(entidadesEnum.PROYECTOS.getTabla());
	}

	@Override
	public void salirLista() {
		getLista().remove(this);

	}
}
