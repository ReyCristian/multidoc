package com.model;

import com.db.DBTabla;
import com.db.DataBaseController;
import com.tool.Listable;
import com.tool.PrinterDate;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class Entrada implements Listable{

	private ObjectProperty<Integer> id;
	private ObjectProperty<PrinterDate> fecha;
	private ObjectProperty<Config> confP_ID;
	private SimpleStringProperty motivo;
	private ObjectProperty<Empleado> empleado;
	private SimpleStringProperty almacen;

	public Entrada(Integer id) {
		super();
		this.id = new SimpleObjectProperty<Integer>(id);
		this.fecha = new SimpleObjectProperty<PrinterDate>(PrinterDate.today());
		this.confP_ID = new SimpleObjectProperty<Config>();
		this.motivo = new SimpleStringProperty();
		this.empleado = new SimpleObjectProperty<Empleado>();
		this.almacen = new SimpleStringProperty();
	}

	public ObjectProperty<Integer> idProperty() {
		return id;
	}

	public ObjectProperty<PrinterDate> fechaProperty() {
		return fecha;
	}

	public ObjectProperty<Config> confP_IDProperty() {
		return confP_ID;
	}

	public SimpleStringProperty motivoProperty() {
		return motivo;
	}

	public ObjectProperty<Empleado> empleadoProperty() {
		return empleado;
	}

	public SimpleStringProperty almacenProperty() {
		return almacen;
	}

	@Override
	public String getStringId() {
		return getId() + " | " + fechaProperty().get();
	}

	@Override
	public Integer getId() {
		return idProperty().get();
	}

	@Override
	public void setId(Integer id) {
		if (this.getId() == null) {
			this.idProperty().set(id);
		}
		
	}

	@Override
	public DBTabla<? extends Listable> getTabla() {
		
		return (DBTabla<Entrada>) DataBaseController.instance().getTabla(entidadesEnum.ENTRADAS.getTabla());
		}

	@Override
	public void salirLista() {

	}
}
