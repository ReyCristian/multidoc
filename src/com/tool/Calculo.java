package com.tool;

public abstract class Calculo {
	private Object resultado;
	private boolean calculado = false;

	public Object calcular() {
		if (!calculado)
			resultado = calc();
		calculado=true;
		return resultado;

	}

	
	protected void setResultado(Object resultado) {
		this.resultado=resultado;
		calculado=true;
	}
	
	public void reset() {
		calculado=false;
	}
	
	protected abstract Object calc();
}
