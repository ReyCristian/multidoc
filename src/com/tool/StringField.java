package com.tool;

import javafx.scene.control.TextField;

public class StringField extends TextField implements ABMio {

	public StringField(String text) {
		super(text);
	}

	public StringField() {
		this("");
	}

	@Override
	public void setEnable(boolean enable) {
		setEditable(enable);
		if (enable) {
			if (getText().equals("-"))
				setText("");
		} else {
			if (getText().equals(""))
				setText("-");
		}
	}

	@Override
	public Object getValor() {
		return getText();
	}

	@Override
	public double getSize() {
		double size = 44d;
		if (getHeight() > size)
			size = getHeight();
		return size;
	}

	@Override
	public boolean isEnable() {
		return this.isEditable();
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString()+this.getValor()+(this.isEditable()?":Editable":":No Editable");
	}

}
