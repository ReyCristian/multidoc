package com.tool;

import java.util.ArrayList;
import java.util.List;

import com.model.Problema;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyEvent;

public class ListField<E> extends TreeView<ABMio> implements ABMio {
	PrinterList<E> lista;
	private boolean enable;
	private TreeItem<ABMio> last;

	public ListField(PrinterList<E> lista) {
		super();
		this.lista = lista;
		setEnable(false);
		

	}

	private void crearLista() {
		for (E e : lista)
			last = addItem(e);

		if (getRoot() == null)
			last = addItem(null);
	}

	private void borrarLista() {
		if (getRoot() != null)
			getRoot().getChildren().clear();
		setRoot(null);

	}

	private void guardarLista() {
		if (getRoot() != null) {
			lista.clear();
			addToList(getRoot());
			for (TreeItem<ABMio> i : getRoot().getChildren()) {
				addToList(i);
			}
		}
	}

	private void addToList(TreeItem<ABMio> i) {
		if (i.getValue().getValor() != null)
			if (lista.getListClass() == String.class && i.getValue().getValor().equals(""))
				;
			else if (lista.getListClass() == Problema.class && i.getValue().getValor().equals(""))
				;
			else
				lista.add((E) i.getValue().getValor());

	}

	private TreeItem<ABMio> addItem(E e) {
		TreeItem<ABMio> treeItem = new TreeItem<ABMio>(ABMio.CrearABMField(e, lista.getListClass()));
		if (getRoot() == null) {
			setRoot(treeItem);
			treeItem.setExpanded(true);
		} else {
			getRoot().getChildren().add(treeItem);
			treeItem.getValue().setEnable(isEnable());
		}
		return treeItem;
	}

	public TreeItem<ABMio> addElement(E e) {
		lista.add(e);
		return addItem(e);
	}

	public PrinterList<E> getLista() {
		return lista;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
		guardarLista();
		borrarLista();
		crearLista();
		getRoot().getValue().setEnable(enable);
		for (TreeItem<ABMio> item : getRoot().getChildren()) {
			item.getValue().setEnable(enable);
		}
		if (enable) {
			last = addElement(null);
			last.getValue().setOnKeyPressed(e -> {
				EventHandler<? super KeyEvent> saved = last.getValue().getOnKeyPressed();
				last.getValue().setOnKeyPressed(new EventHandler<Event>() {

					@Override
					public void handle(Event arg0) {
						
					}
				});
				last = addElement(null);
				last.getValue().setOnKeyPressed(saved);
			});
		}
	}

	@Override
	public Object getValor() {
		guardarLista();
		return getLista();
	}

	@Override
	public double getSize() {
		double size = 10d+ 50d * (lista.size() + (isEnable()?0:1));
//		if (getHeight() > size)
//			size = getHeight();
		return size;
	}

	@Override
	public boolean isEnable() {
		return enable;
	}

	public TreeItem<ABMio> getLast() {
		return last;
	}
	

}
