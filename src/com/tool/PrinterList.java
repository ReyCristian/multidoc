package com.tool;

import java.util.ArrayList;
import java.util.List;

public class PrinterList<E> extends ArrayList<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ListField<E> treeView;
	private Class<?> listClass;

	public PrinterList() {
		super();
	}

	public PrinterList(E e) {
		this();
		add(e);
	}

	@Override
	public String toString() {
		if (isEmpty())
			return "-";
		if (get(0)==null)
			return "-";
		return get(0).toString();
	}

	public String fullString() {
		return super.toString();
	}

	public ListField<E> getTreeView() {
		if (treeView == null)
			treeView = new ListField<E>(this);
		return treeView;
	}

	public Class<?> getListClass() {
		return this.listClass;
	}

	public void setListClass(Class<?> listClass) {
		this.listClass = listClass;
	}

}
