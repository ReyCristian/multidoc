package com.tool;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.AccessibleRole;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.util.Callback;

public class DateField extends DatePicker implements ABMio {

	private Callback<DatePicker, DateCell> factoryEnable;
	private Callback<DatePicker, DateCell> factoryDisable;

	public DateField() {
		super();
		
		factoryEnable = getDayCellFactory();
		factoryDisable = dp -> new DateCell() {
			@Override
			public void updateItem(LocalDate item, boolean empty) {
				super.updateItem(item, empty);

				if (item.isBefore(getValue()) || item.isAfter(getValue())) {
					setStyle("-fx-background-color: #ffc0cb;");
					Platform.runLater(() -> setDisable(true));

				}
			}
		};

		
	}

	@Override
	public void setEnable(boolean enable) {
		setEditable(enable);
		if (enable)
			setDayCellFactory(factoryEnable);
		else
			setDayCellFactory(factoryDisable);
	}

	@Override
	public boolean isEnable() {
		return isEditable();
	}

	@Override
	public double getSize() {
		return 44d;
	}

	@Override
	public PrinterDate getValor() {
		Date fecha = Date.from(super.getValue().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
		return new PrinterDate(fecha.getTime());
	}

}
