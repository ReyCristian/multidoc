package com.tool;

import javafx.scene.control.TextField;
import javafx.scene.layout.Background;

public class CalculatedField extends TextField implements ABMio {

	private Calculo calculo;
	private Background enableBackground;
	
	public CalculatedField() {
		this(null);
	}

	public CalculatedField(Object atributo) {
		super(atributo.toString());
		setEditable(false);
		enableBackground=getBackground();
	}

	@Override
	public void setEnable(boolean enable) {
		setDisable(enable);
		if (!enable && calculo !=null)
			if (calculo.calcular()!=null)
				setText(calculo.calcular().toString());
	}

	public void setCalculo(Calculo calculo) {
		this.calculo = calculo;
	}

	@Override
	public Object getValor() {
		if (calculo == null)
			return null;
		return calculo.calcular();
	}
	@Override
	public double getSize() {
		double size = 44d;
		if (getHeight()>size)
			size= getHeight();
		return size;
	}

	@Override
	public boolean isEnable() {
		return this.isDisable();
	}

	public void recalcular() {
		calculo.reset();
	}
	
	
}