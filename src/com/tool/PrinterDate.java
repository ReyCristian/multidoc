package com.tool;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class PrinterDate extends Date {


	public PrinterDate(long date) {
		super(date);
	}

	public PrinterDate() {
		this(0);
	}

	@Override
	public String toString() {
		return String.format("%tF", this);
	}

	public static PrinterDate today() {
		PrinterDate today = new PrinterDate(Calendar.getInstance().getTime().getTime());
		return today;
	}

	public LocalDate getLocalDate() {
		return this.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}
}
