package com.tool;

import com.model.Cliente;
import com.model.Config;
import com.model.Empleado;
import com.model.Persona;
import com.model.Producto;
import com.model.Proyecto;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.KeyEvent;

public interface ABMio {
	public void setEnable(boolean enable);

	public boolean isEnable();

	public Object getValor();
	
	public void setOnKeyPressed(EventHandler<? super KeyEvent> e);
	public EventHandler<? super KeyEvent> getOnKeyPressed();

	public static ABMio CrearABMField(Object atributo, Class<?> clase) {
		return (ABMio) CrearABMNode(atributo, clase);
	}

	public static Node CrearABMNode(Object atributo, Class<?> clase) {
		Node retorno;
		boolean nulo=false;
		if (atributo == null) {
			atributo = new String("");
			nulo=true;
		}
		if (clase == Calculo.class) {
			CalculatedField calculando = new CalculatedField(atributo);
			Calculo calculo = new Calculo() {
				
				@Override
				protected Object calc() {
					try {
					return Integer.parseInt(calculando.getText());
					}catch (Exception e) {
						return null;
					}
				}
			};
			if (!atributo.equals(""))
				try {
					calculo.setResultado((Integer) atributo);
				} catch (Exception e) {
					e.printStackTrace();
				}
			calculando.setCalculo(calculo);
			retorno = calculando;
		} else if (atributo.getClass() == PrinterList.class || clase == PrinterList.class) {
			ListField atributoTree = ((PrinterList) atributo).getTreeView();
			retorno = atributoTree;
		} else if (atributo.getClass() == Integer.class || clase == Integer.class) {

			IntField atributoField = new IntField(atributo);
			atributoField.setEnable(false);
			retorno = atributoField;

		} else if (clase == Producto.class) {
			TextSelector<Producto> atributoField = new TextSelector<Producto>();
			Producto.getLista().forEach(e -> atributoField.getLista().put(e.getStringId(), e));
			atributoField.setEnable(false);
			if (!nulo)
				atributoField.setText(((Listable)atributo).getStringId());
			retorno = atributoField;
		} else if (clase == Empleado.class) {
			TextSelector<Persona> atributoField = new TextSelector<Persona>();
			Empleado.getLista().forEach(e -> atributoField.getLista().put(e.getStringId(), e));
			atributoField.setEnable(false);
			if (!nulo)
				atributoField.setText(((Listable)atributo).getStringId());
			retorno = atributoField;
		} else if (clase == Cliente.class) {
			TextSelector<Persona> atributoField = new TextSelector<Persona>();
			Cliente.getLista().forEach(e -> atributoField.getLista().put(e.getStringId(), e));
			atributoField.setEnable(false);
			if (!nulo)
				atributoField.setText(((Listable)atributo).getStringId());
			retorno = atributoField;

		} else if (clase == Config.class) {
			TextSelector<Config> atributoField = new TextSelector<Config>();
			Config.getLista().forEach(e -> atributoField.getLista().put(e.getStringId(), e));
			atributoField.setEnable(false);
			
			if (!nulo)
				atributoField.setText(((Listable)atributo).getStringId());
			retorno = atributoField;
		} else if (clase == Proyecto.class) {
			TextSelector<Proyecto> atributoField = new TextSelector<Proyecto>();
			Proyecto.getLista().forEach(e -> atributoField.getLista().put(e.getStringId(), e));
			atributoField.setEnable(false);
			if (!nulo)
				atributoField.setText(((Listable)atributo).getStringId());
			retorno = atributoField;
		} else if (atributo.getClass() == PrinterDate.class || clase == PrinterDate.class) {
			DateField atributoField = new DateField();
			if (atributo.getClass() == PrinterDate.class)
				atributoField.setValue(((PrinterDate) atributo).getLocalDate());
			atributoField.setEnable(false);
			retorno = atributoField;
		} else {
			StringField atributoField = new StringField(atributo.toString());
			atributoField.setEnable(false);
			retorno = atributoField;
		}
		
		return retorno;
	}

	public double getSize();
}
