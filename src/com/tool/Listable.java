package com.tool;

import java.util.ArrayList;
import java.util.List;

import com.db.DBTabla;

public interface Listable extends Indexado{

	public String getStringId();

	public void setId(Integer id);
	public DBTabla<? extends Listable> getTabla();
	public void salirLista();
	
}
