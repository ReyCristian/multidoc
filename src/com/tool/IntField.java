package com.tool;

import javafx.scene.control.TextField;

public class IntField extends TextField implements ABMio {

	public IntField(Integer atributo) {
		super(atributo.toString());
	}

	public IntField(Object atributo) {
		super(atributo.toString());
	}

	public IntField() {
		super();
	}

	@Override
	public void setEnable(boolean enable) {
		setEditable(enable);
		if (enable) {
			if (getText().equals("#"))
				setText("");
		} else {
			if (getText().equals(""))
				setText("#");
		}
	}

	@Override
	public Object getValor() {
		if (this.getText().equals("") || this.getText().equals("#"))
			return null;
		return Integer.parseInt(this.getText());
	}

	@Override
	public double getSize() {
		double size = 44d;
		if (getHeight() > size)
			size = getHeight();
		return size;
	}

	@Override
	public boolean isEnable() {
		return this.isEditable();
	}
}
