package com.launcher;

import java.io.IOException;
import java.lang.reflect.Method;

import com.db.DataBaseController;
import com.model.entidadesEnum;
import com.view.ABMController;
import com.view.LoginController;
import com.view.VentanaController;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

public class Main extends Application {

	private Stage primaryStage;

	private AnchorPane login;
	private Stage loginStage;

	private BorderPane root;

	private Image logo;

	@Override
	public void start(Stage primaryStage) {

		this.primaryStage = primaryStage;
		launchLogin();
		
		for (entidadesEnum ent : entidadesEnum.values())
		try {
			DataBaseController.instance().getTabla(ent.getTabla()).leer();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void launchLogin() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(ABMController.class.getResource("Login.fxml"));

			login = (AnchorPane) loader.load();

			LoginController controller = loader.getController();

			controller.setMain(this);

			loginStage = new Stage();
			loginStage.setScene(new Scene(login));
			loginStage.setResizable(false);
			loginStage.initStyle( StageStyle.UNDECORATED );
			logo = controller.getLogo();
			loginStage.getIcons().add(logo);
			loginStage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		launch(args);
	}

	public void launchPrincipal(int i) {
		try {

			loginStage.close();
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(ABMController.class.getResource("Ventana"+i+".fxml"));

			root = (BorderPane) loader.load();
			VentanaController controller = loader.getController();

			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.setMaximized(true);
			primaryStage.getIcons().add(logo);
			primaryStage.show();
			primaryStage.setOnCloseRequest(e->launchLogin());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void closeLogin() {
		loginStage.close();
		System.exit(0);
		
	}

}
