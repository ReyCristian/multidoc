package com.db;

import java.sql.ResultSet;
import java.util.List;

import com.model.Cliente;

public class TablaClientes extends TablaPersona<Cliente> {
	final static String nombreTabla = "Cliente";
	
	public TablaClientes() {
		super(nombreTabla);

		// Cliente_ID Nombre Dirección
		this.addColumnas(new Columna("Cliente_ID", TipoDatoEnum.INT, 11, "NOT NULL"));
		this.addColumnas(new Columna("Nombre", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Dirección", TipoDatoEnum.VARCHAR, 45, "NULL"));

	
		
	}

	@Override
	public Cliente interpretar(ResultSet rs) throws Exception {
		Cliente cliente = new Cliente(rs.getInt(getColumna(0)));
		cliente.nombreProperty().set(rs.getString(getColumna(1)));
		cliente.direccionProperty().set(rs.getString(getColumna(2)));
		
		cargarDatos(cliente);
		
		
		
		return cliente;
	}

	@Override
	public String registrotoString(Cliente registro) {
		return registro.getId() + ",'" + registro.getNombre() + "', '" + registro.getDireccion() + "'";
	}

}
