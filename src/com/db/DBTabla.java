package com.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tool.Indexado;
import com.tool.Listable;


public abstract class DBTabla<T> {
	protected DataBase database;
	private String nombre;
	private List<Columna> columnas;
	private String primaryKey;

	public DBTabla(String nombre) {
		super();
		this.database =  new DataBase();;
		this.nombre = nombre;

		columnas = new ArrayList<Columna>();

	}

	public DataBase getDatabase() {
		return database;
	}

	public void setDatabase(DataBase database) {
		this.database = database;
	}

	public String getNombre() {
		return nombre;
	}

	public List<Columna> getColumnas() {
		return columnas;
	}

	public void addColumnas(Columna columna) {
		this.columnas.add(columna);
	}

	public String getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	public boolean crearDB() throws Exception {
		try {
			if (primaryKey == null)
				throw new SQLException("No se ha seleccionado PrimaryKey");
			return database.createTable(this);

		} catch (Exception e) {
			throw e;
		}

	}

	public List<T> leer() throws Exception {
		return buscar("1");
	}
	
	public List<T> buscar(String filtro) throws Exception {
		List<T> tabla = new ArrayList<T>();
		ResultSet rs = getDatabase().buscarEnTabla(getNombre(), filtro);
		while (rs.next()) {
			T registro = interpretar(rs);
			tabla.add(registro);
		}
		return tabla;
	}
	
	public String getColumna(int index) {
		return getColumnas().get(index).getName();
	}
	public abstract T interpretar(ResultSet rs) throws Exception;
	
	public Integer agregarRegistro(T registro) throws SQLException,NullPointerException{
		String sql = "INSERT INTO `"+nombre+"` ("+getListColumnas()+") VALUES ("+registrotoString(registro)+")";
		PreparedStatement stmt = getDatabase().conectarMySQL().prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
		System.out.println(sql);
		stmt.execute();
		ResultSet generatedKeys = stmt.getGeneratedKeys();
		Integer id = null;
		if (generatedKeys.next()) 
		         id= generatedKeys.getInt(1);
		return id;
	}
	
	public void borrar(String filtro) throws SQLException {
		String sql = "DELETE FROM `"+ nombre + "` WHERE " + filtro;
		System.out.println(sql);
		Statement stmt = getDatabase().conectarMySQL().createStatement();
		stmt.execute(sql);
	}
	
	public abstract String registrotoString(T registro)throws NullPointerException;
	
	protected String getListColumnas() {
		String r="";
		for (Columna columna:columnas) {
			r+="`"+columna.getName()+"`";
			if(columna!=columnas.get(columnas.size()-1))
				r+=", ";
		}
		return r;
	}
	
	
}
