package com.db;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.List;

import com.model.Cliente;
import com.model.Config;
import com.model.Empleado;
import com.model.Insumo;
import com.model.Producto;
import com.model.Proyecto;
import com.model.Puerto;
import com.model.Servicio;
import com.tool.PrinterDate;

public class TablaServicios extends TablaIndexado<Servicio> {
	final static String nombreTabla = "Servicio";
	private TablaSub subTablaEmpleado;
	private TablaSub subTablaProducto;
	private TablaMultiSub subTablaInsumos;

	public TablaServicios() {
		super(nombreTabla);

		// Servicio_Numero Fecha Cliente_ID Proyecto_ID Responsable Vehículo
		// Observaciones
		this.addColumnas(new Columna("Servicio_Numero", TipoDatoEnum.INT, 11, "NOT NULL"));
		this.addColumnas(new Columna("Fecha", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Cliente_ID", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Proyecto_ID", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Vehículo", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Responsable", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Observaciones", TipoDatoEnum.VARCHAR, 45, "NULL"));

		subTablaEmpleado = new TablaSub(nombreTabla + "_Empleado");
		subTablaProducto = new TablaSub(nombreTabla + "_Productos");
		subTablaInsumos = new TablaMultiSub(nombreTabla + "_Insumos", Insumo.size);

	}

	@Override
	public Servicio interpretar(ResultSet rs) throws Exception {
		int id = rs.getInt(getColumna(0));
		Servicio servicio = new Servicio(id);
		servicio.fechaProperty()
				.set(new PrinterDate(new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString(getColumna(1))).getTime()));
		servicio.clienteProperty().set(Cliente.get(rs.getInt(getColumna(2))));
		servicio.proyectoProperty().set(Proyecto.get(rs.getInt(getColumna(3))));
		servicio.vehiculoProperty().set(rs.getString(getColumna(4)));
		servicio.responsableProperty().set(rs.getString(getColumna(5)));
		servicio.observacionesProperty().set(rs.getString(getColumna(6)));

		List<String> empleados = subTablaEmpleado.buscar(getColumna(0) + " = " + id);
		for (String empleado:empleados) {
		servicio.empleadoProperty().get().add(Empleado.get(Integer.parseInt(empleado)));
		}
		
		servicio.equiposProperty().get().addAll(Config.parse(subTablaProducto.buscar(getColumna(0) + " = " + id)));
		
		servicio.materialesProperty().get().addAll(Insumo.parse(subTablaInsumos.buscar(getColumna(0) + " = " + id)));
		
		
		return servicio;
	}

	@Override
	public String registrotoString(Servicio registro) {
		
		return registro.idProperty().get() + ",'" + registro.fechaProperty().get().toString() + "', "
				+ registro.clienteProperty().get().getId() + ", "
				+ registro.proyectoProperty().get().idProperty().get() + ", '" + registro.responsableProperty().get()
				+ "','" + registro.vehiculoProperty().get() + "','" + registro.observacionesProperty().get() + "'";
	}

}
