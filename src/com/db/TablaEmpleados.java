package com.db;

import java.sql.ResultSet;

import com.model.Empleado;

public class TablaEmpleados extends TablaPersona<Empleado> {
	final static String nombreTabla = "Empleado";

	public TablaEmpleados() {
		super(nombreTabla);
		
		//Empleado_ID 	DNI 	Nombre 	Apellido
		this.addColumnas(new Columna("Empleado_ID", TipoDatoEnum.INT, 11, "NOT NULL"));
		this.addColumnas(new Columna("DNI", TipoDatoEnum.INT, 10, "NULL"));
		this.addColumnas(new Columna("Nombre", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Apellido", TipoDatoEnum.VARCHAR, 45, "NULL"));

		
		
	}

	@Override
	public Empleado interpretar(ResultSet rs) throws Exception {
		Empleado empleado = new Empleado(rs.getInt(getColumna(0)));
		empleado.DNIProperty().set(rs.getInt(getColumna(1)));
		empleado.nombreProperty().set(rs.getString(getColumna(2)));
		empleado.apellidoProperty().set(rs.getString(getColumna(3)));
		
		cargarDatos(empleado);
		
		return empleado;
	}

	@Override
	public String registrotoString(Empleado registro) {
		return registro.getId()+",'" + registro.getDNI() + "', '" + registro.getNombre() + "', '" + registro.apellidoProperty().get()+ "'";
	}

}
