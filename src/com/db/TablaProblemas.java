package com.db;

import java.sql.ResultSet;
import java.util.Collection;

import com.model.Problema;
import com.model.Solucion;

public class TablaProblemas extends DBTabla<Problema> {

	private Integer id = null;
	private TablaMultiSub subTablaSoluciones;

	public TablaProblemas(String nombre) {
		super(nombre);
		
		//Problema_ID 	ConfP_ID 	Problema
		this.addColumnas(new Columna("Problema_ID", TipoDatoEnum.INT, 11, "NOT NULL"));
		this.addColumnas(new Columna("ConfP_ID", TipoDatoEnum.INT, 11, "NOT NULL"));
		this.addColumnas(new Columna("Problema", TipoDatoEnum.VARCHAR, 45, "NULL"));

		subTablaSoluciones = new TablaMultiSub(nombre + "_soluciones",Solucion.size);
		
		
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public Problema interpretar(ResultSet rs) throws Exception {
		int id = rs.getInt(getColumna(0));
		Problema problema = new Problema(id);
		problema.setIdProd(rs.getInt(getColumna(1)));
		problema.set(rs.getString(getColumna(2)));
		
		Collection<? extends Solucion> lista = Solucion.parse(subTablaSoluciones.buscar(getColumna(0) + " = " + id));
		System.out.println(lista);
		
		problema.getSoluciones().addAll(Solucion.parse(subTablaSoluciones.buscar(getColumna(0) + " = " + id)));
		
		
		return problema;
		
	}

	@Override
	public String registrotoString(Problema registro) {
		
		return null;
	}

}
