package com.db;

import java.sql.ResultSet;
import com.model.Producto;

public class TablaProductos extends TablaIndexado<Producto> {
	final static String nombreTabla = "Producto";
	private TablaSub subTablaManual;
	private TablaSub subTablaObservaciones;

	public TablaProductos() {
		super(nombreTabla);
		
		//Prod_ID 	Modelo 	Tipo_Producto 	Marca 	Caracteristica
		this.addColumnas(new Columna("Prod_ID", TipoDatoEnum.INT, 11, "NOT NULL"));
		this.addColumnas(new Columna("Modelo", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Tipo_Producto", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Marca", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Caracteristica", TipoDatoEnum.VARCHAR, 45, "NULL"));

		subTablaManual = new TablaSub(nombreTabla+"_Manual");
		subTablaObservaciones = new TablaSub(nombreTabla+"_Observaciones");
				
		
	}

	@Override
	public Producto interpretar(ResultSet rs) throws Exception {
		int id = rs.getInt(getColumna(0));
		Producto producto = new Producto(id);
		producto.modeloProperty().set(rs.getString(getColumna(1)));
		producto.tipoProductoProperty().set(rs.getString(getColumna(2)));
		producto.marcaProperty().set(rs.getString(getColumna(3)));
		producto.caracteristicaProperty().set(rs.getString(getColumna(4)));

		producto.manualesProperty().get().addAll(subTablaManual.buscar(getColumna(0) + " = " + id));
		producto.observacionesProperty().get().addAll(subTablaObservaciones.buscar(getColumna(0) + " = " + id));
		
		return producto;
	}

	@Override
	public String registrotoString(Producto registro) {
		return registro.getId()+",'" + registro.getModelo() + "', '" + registro.getTipoProducto() + "', '" + registro.getMarca() + "', '"
				+ registro.getCaracteristica() + "'";
	}

}
