package com.db;

import java.sql.SQLException;
import java.util.List;

import com.model.Cliente;
import com.model.Persona;
import com.tool.Listable;

public abstract class TablaPersona<T extends Persona> extends TablaIndexado<T> {

	private TablaSub subTablaEmail;
	private TablaSub subTablaTelefono;

	public TablaPersona(String nombre) {
		super(nombre);

		subTablaEmail = new TablaSub(nombre + "_Email");
		subTablaTelefono = new TablaSub(nombre + "_Tel");

	}

	protected void cargarDatos(Persona persona) throws Exception {

		persona.emailProperty().get().addAll(subTablaEmail.buscar(getFiltro(persona)));
		List<String> listaTel = subTablaTelefono.buscar(getFiltro(persona));

		for (String telefono : listaTel) {
			persona.addTelefono(Integer.parseInt(telefono));
		}

	}

	protected String getFiltro(Persona persona) {
		return getColumna(0) + " = " + persona.getId();
	}
	
	protected String getFiltro(Integer id) {
		return getColumna(0) + " = " + id;
	}

	@Override
	public Integer agregarRegistro(T registro) throws SQLException, NullPointerException {
		Integer id = super.agregarRegistro(registro);

		subTablaEmail.setId(id);
		subTablaTelefono.setId(id);
		
		subTablaEmail.borrar(getFiltro(id));
		subTablaTelefono.borrar(getFiltro(id));
		
		registro.emailProperty().get().forEach(email -> {

			try {
				subTablaEmail.agregarRegistro(email);
			} catch (NullPointerException | SQLException e) {
				e.printStackTrace();
			}
		});

		registro.telefonoProperty().get().forEach(tel -> {

			try {
				
				subTablaTelefono.agregarRegistro(tel.toString());
			} catch (NullPointerException | SQLException e) {
				e.printStackTrace();
			}
		});

		return id;
	}

}
