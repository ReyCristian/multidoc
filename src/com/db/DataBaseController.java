package com.db;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.model.entidadesEnum;

public class DataBaseController {

	private static DataBaseController _instance = null;

	private Map<String, DBTabla<?>> tablas;
	
	
	public static DataBaseController instance() {
		if (_instance == null)
			_instance = new DataBaseController();
		return _instance;
	}

	public DataBaseController() {
		tablas = new HashMap<String, DBTabla<?>>();
		tablas.put(entidadesEnum.CLIENTES.getTabla(),new TablaClientes());
		tablas.put(entidadesEnum.PRODUCTOS.getTabla(),new TablaProductos());
		tablas.put(entidadesEnum.SERVICIOS.getTabla(),new TablaServicios());
		tablas.put(entidadesEnum.EMPLEADOS.getTabla(),new TablaEmpleados());
		tablas.put(entidadesEnum.PROYECTOS.getTabla(),new TablaProyectos());
		tablas.put(entidadesEnum.CONFIGURACION.getTabla(),new TablaConfig());
		tablas.put(entidadesEnum.ENTRADAS.getTabla(),new TablaEntradas());
		
	}

	public DBTabla<?> getTabla(String tabla) {
		return tablas.get(tabla);
	}
	
	public List<?> get(String nombreTabla,int id) {
		DBTabla<?> tabla = getTabla(nombreTabla);
		List<?> l=null;
		try {
			l = tabla.buscar(tabla.getColumna(0) + " = " + id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l;
	}
	
}
