package com.db;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.List;

import com.model.Config;
import com.model.Empleado;
import com.model.Entrada;
import com.model.Persona;
import com.model.Producto;
import com.model.Usuario;
import com.tool.PrinterDate;

public class TablaUsuarios extends TablaPersona<Usuario> {
	final static String nombreTabla = "Usuarios";
	private TablaMultiSub subTablaDatos;
	private TablaMultiSub subTablaPermisos;

	public TablaUsuarios() {
		super(nombreTabla);

		// ConfP_ID NombreUsuario Contraseña
		this.addColumnas(new Columna("ConfP_ID", TipoDatoEnum.INT, 11, "NOT NULL"));
		this.addColumnas(new Columna("NombreUsuario", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Contraseña", TipoDatoEnum.VARCHAR, 45, "NULL"));

		subTablaDatos = new TablaMultiSub(nombreTabla + "_Datos", 4);
		subTablaPermisos = new TablaMultiSub(nombreTabla + "_Permisos", 2);

	}

	@Override
	public Usuario interpretar(ResultSet rs) throws Exception {
		Usuario usuario = new Usuario(rs.getInt(getColumna(0)));
		usuario.getNombreUsuario().set(rs.getString(getColumna(1)));
		usuario.getPass().set(rs.getString(getColumna(2)));

		List<String[]> datos = subTablaDatos.buscar(getFiltro(usuario));
		if (datos.size() > 0) {
			usuario.getSector().set(datos.get(0)[1]);
			usuario.getPuesto().set(datos.get(0)[2]);
			usuario.setNombre(datos.get(0)[3]);
		}

		cargarDatos(usuario);

		List<String[]> permisos = subTablaPermisos.buscar(getFiltro(usuario));
		permisos.forEach(permiso -> {
			usuario.getPermisos().get().add(permiso[1]);
		});

		return usuario;
	}

	@Override
	public String registrotoString(Usuario registro) {
//		return "'" + registro.idProperty().get() + "','" + registro.fechaProperty().get() + "','"
//				+ registro.confP_IDProperty().get() + "','" + registro.motivoProperty().get() + "','"
//				+ registro.empleadoProperty().get() + "','" + registro.almacenProperty().get() + "'";
		return null;
	}

	@Override
	protected String getFiltro(Persona usuario) {
		// TODO Auto-generated method stub
		return "(" + getColumna(0) + " = " + usuario.getId() + " & " + getColumna(1) + " = '"
				+ ((Usuario) usuario).getNombreUsuario().get() + "')";
	}
}
