package com.db;

import java.sql.ResultSet;
import com.model.Proyecto;

public class TablaProyectos extends TablaIndexado<Proyecto> {
	final static String nombreTabla = "Proyecto";
	private TablaSub subTablaITecnico;
	private TablaSub subTablaObservaciones;
	private TablaSub subTablaPlanos;

	public TablaProyectos() {
		super(nombreTabla);
		
		//Proyecto_ID 	Nombre
		this.addColumnas(new Columna("Proyecto_ID", TipoDatoEnum.INT, 11, "NOT NULL"));
		this.addColumnas(new Columna("Nombre", TipoDatoEnum.VARCHAR, 45, "NULL"));
		
		subTablaITecnico = new TablaSub(nombreTabla+"_iTecnico");
		subTablaObservaciones = new TablaSub(nombreTabla+"_Observaciones");
		subTablaPlanos = new TablaSub(nombreTabla+"_Planos");
		
		
	}

	@Override
	public Proyecto interpretar(ResultSet rs) throws Exception {
		int id = rs.getInt(getColumna(0));
		Proyecto proyecto = new Proyecto(id);
		proyecto.nombreProperty().set(rs.getString(getColumna(1)));
		
		
		proyecto.informesProperty().get().addAll(subTablaITecnico.buscar(getColumna(0) + " = " + id));
		proyecto.observacionesProperty().get().addAll(subTablaObservaciones.buscar(getColumna(0) + " = " + id));
		proyecto.planosProperty().get().addAll(subTablaPlanos.buscar(getColumna(0) + " = " + id));
		
		
		
		return proyecto;
	}

	@Override
	public String registrotoString(Proyecto registro) {
		return registro.idProperty().get()+",'" + registro.nombreProperty().get() + "'";
	}

}
