package com.db;

import java.sql.ResultSet;

public class TablaSub extends DBTabla<String> {

	public TablaSub(String nombre) {
		super(nombre);
	}

	private Integer id = null;

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String interpretar(ResultSet rs) throws Exception {
		return rs.getString(2);
	}

	@Override
	public String registrotoString(String registro) {
		if (id == null)
			return null;
		return id + ",'" + registro + "'";
	}

}
