package com.db;

import java.sql.ResultSet;

public class TablaMultiSub extends DBTabla<String[]> {

	private Integer id = null;
	private Integer size = null;

	public TablaMultiSub(String nombre, int size) {
		super(nombre);
		this.size = size;

	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String[] interpretar(ResultSet rs) throws Exception {
		String[] retorno = new String[size];
		for (int i=0;i<size;i++)
			retorno[i]=rs.getString(2+i);
		return retorno;
	}

	@Override
	public String registrotoString(String[] registro) {
		if (id == null)
			return null;
		String retorno = "'" + id;
		for (String fila : registro)
			retorno += "','"+ fila;
		retorno += "'";
		return retorno;
	}

}
