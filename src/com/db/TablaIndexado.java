package com.db;

import java.sql.SQLException;
import java.sql.Statement;

import com.tool.Indexado;

public abstract class TablaIndexado<T extends Indexado> extends DBTabla<T> {

	public TablaIndexado(String nombre) {
		super(nombre);
	}

	@Override
	public Integer agregarRegistro(T registro) throws SQLException, NullPointerException {
		if (registro.getId() == null)
			return super.agregarRegistro(registro);
		String sql = "UPDATE `" + getNombre() + "` SET " + getUpdateDataList(registrotoString(registro).split(","))
				+ " WHERE `" + getColumna(0) + "`=" + registro.getId();

		System.out.println(sql);
		Statement stmt = getDatabase().conectarMySQL().createStatement();
		int r = stmt.executeUpdate(sql);
		return registro.getId();
	}

	private String getUpdateDataList(String[] datos) {
		String sql = "";
		for (int i = 0; i < datos.length; i++) {
			sql += "`" + getColumna(i) + "`=" + datos[i];
			if (i < (datos.length - 1))
				sql += ",";
		}
		return sql;

	}

}