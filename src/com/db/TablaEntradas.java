package com.db;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import com.model.Config;
import com.model.Empleado;
import com.model.Entrada;
import com.model.Producto;
import com.tool.PrinterDate;

public class TablaEntradas extends TablaIndexado<Entrada> {
	final static String nombreTabla = "Entrada_Labortorio";

	public TablaEntradas() {
		super(nombreTabla);

		// Entrada_ID Fecha_Entrada ConfP_ID Motivo Empleado_ID Almacen
		this.addColumnas(new Columna("Entrada_ID", TipoDatoEnum.INT, 11, "NOT NULL"));
		this.addColumnas(new Columna("Fecha_Entrada", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("ConfP_ID", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Motivo", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Empleado_ID", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Almacen", TipoDatoEnum.VARCHAR, 45, "NULL"));

	}

	@Override
	public Entrada interpretar(ResultSet rs) throws Exception {
		int id = rs.getInt(getColumna(0));
		Entrada entrada = new Entrada(id);
		entrada.fechaProperty()
				.set(new PrinterDate(new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString(getColumna(1))).getTime()));
		entrada.confP_IDProperty().set(Config.get(rs.getInt(getColumna(2))));
		entrada.motivoProperty().set(rs.getString(getColumna(3)));
		entrada.empleadoProperty().set(Empleado.get(rs.getInt(getColumna(4))));
		entrada.almacenProperty().set(rs.getString(getColumna(5)));

		return entrada;
	}

	@Override
	public String registrotoString(Entrada registro) throws NullPointerException{
		// Entrada_ID Fecha_Entrada ConfP_ID Motivo Empleado_ID Almacen
		return registro.idProperty().get()+",'" +registro.fechaProperty().get()+"'," +
		registro.confP_IDProperty().get().getId()+",'" +registro.motivoProperty().get() +"'," +
		registro.empleadoProperty().get().getId()+",'" +registro.almacenProperty().get()+"'";
		
	}

}
