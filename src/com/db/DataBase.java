package com.db;
 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class DataBase {
 
	private static String name="MultiDoc";

	// Librería de MySQL
	private String driver;

	// Nombre de la base de datos
	private String database;

	private String hostname;
	private String port;

	private String username ;
	private String password;

	Connection conn = null;

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public DataBase(String database) {
		super();
		this.database = database;

		driver = "com.mysql.cj.jdbc.Driver";

		hostname = "localhost";
		port = "3306";

		username = "cesit";
		password = "cesit";

	}

	public DataBase() {
		this(name);
	}
	public static void setName(String name) {
		DataBase.name = name;
	}

	public Connection conectarMySQL() {
		try {
			conn= tryConectarMySQL();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		return conn;

	}
	
	public Connection tryConectarMySQL() throws ClassNotFoundException, SQLException {
		desconectarMySQL();
		String url = "jdbc:mysql://" + hostname + ":" + port + "/" + database + "?useSSL=false&serverTimezone=UTC";
		Class.forName(driver);
		conn = DriverManager.getConnection(url, username, password);
		return conn;

	}

	private boolean desconectarMySQL() {
		try {

			if (conn != null)
				conn.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			return false;
		}
	}

	public boolean createTable(DBTabla<?> table) {
		if (!existTable(table.getNombre()))
			return createTable(table.getNombre(), table.getColumnas(), table.getPrimaryKey());
		else
			return false;
	}

	public boolean createTable(String table, List<Columna> columnas, String primaryKey) {
		System.out.println("creando tabla '" + table + "'");
		String sql = "CREATE TABLE `" + database + "`.`" + table + "` ( ";

		String sColumnas = columnas.toString();
		sql += sColumnas.substring(1, sColumnas.length() - 2);

		sql += " , PRIMARY KEY (`" + primaryKey + "`)) ENGINE = MyISAM;";

		try {

			conectarMySQL();
			Statement stmt = conn.createStatement();
			System.out.println(sql);
			boolean rs = stmt.execute(sql);
			return !rs;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} finally {

			desconectarMySQL();
		}

	}

	public boolean existTable(String table) {

		conectarMySQL();
		String sSQL = "SELECT * FROM `" + table + "` ";
		try {
			Statement stmt = conn.createStatement();
			stmt.executeQuery(sSQL);
			return true;
		} catch (SQLException e) {
		} finally {
			desconectarMySQL();
		}
		return false;
	}

	public ResultSet leerTabla(String nombre) throws SQLException {
		return buscarEnTabla(nombre, "1");

	}

	public ResultSet buscarEnTabla(String nombre, String filtro) throws SQLException {

		conectarMySQL();
		String sql = "SELECT * FROM `" + nombre + "` WHERE " + filtro;
		System.out.println(sql);
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		return rs;
	}
}
