package com.db;

import java.sql.ResultSet;

import com.model.Conexion;
import com.model.Config;
import com.model.Funcion;
import com.model.IP;
import com.model.Producto;
import com.model.Puerto;

public class TablaConfig extends TablaIndexado<Config> {
	final static String nombreTabla = "Conf";
	private TablaSub subTablaRespaldo;
	private TablaSub subTablaImp;
	private TablaMultiSub subTablaFunciones;
	private TablaMultiSub subTablaIP;
	private TablaMultiSub subTablaPuertos;
	private TablaUsuarios subTablaUsuarios;
	private TablaMultiSub subTablaConexion;
	private TablaProblemas subTablaProblemas;
	
	public TablaConfig() {
		super(nombreTabla);
		//ConfP_ID 	Serial 	Prod_ID 	Nombre 	Versión 	Modo 	Observaciones 
		
		this.addColumnas(new Columna("ConfP_ID", TipoDatoEnum.INT, 11, "NOT NULL"));
		this.addColumnas(new Columna("Serial", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Prod_ID", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Nombre", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Versión", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Modo", TipoDatoEnum.VARCHAR, 45, "NULL"));
		this.addColumnas(new Columna("Observaciones", TipoDatoEnum.VARCHAR, 45, "NULL"));
		
		subTablaRespaldo= new TablaSub(nombreTabla+"_Respaldo");
		subTablaImp= new TablaSub(nombreTabla+"_Imppnt");
		
		subTablaFunciones= new TablaMultiSub(nombreTabla+"_Funciones",Funcion.size);
		subTablaIP= new TablaMultiSub(nombreTabla+"_IP",IP.size);
		subTablaPuertos= new TablaMultiSub(nombreTabla+"_Puertos",Puerto.size);
		
		subTablaUsuarios = new TablaUsuarios();
		
		subTablaConexion = new TablaMultiSub("Conexiones", 2);
		
		subTablaProblemas = new TablaProblemas(nombreTabla+"_problemas");
		
	}

	@Override
	public Config interpretar(ResultSet rs) throws Exception {
		int id = rs.getInt(getColumna(0));
		Config conf = new Config(id);
		conf.serialProperty().set(rs.getString(getColumna(1)));
		conf.productoProperty().set(Producto.get(rs.getInt(getColumna(2))));
		conf.nombreProperty().set(rs.getString(getColumna(3)));
		conf.versionProperty().set(rs.getString(getColumna(4)));
		conf.modoProperty().set(rs.getString(getColumna(5)));
		conf.observacionesProperty().set(rs.getString(getColumna(6)));
		
		conf.respaldosProperty().get().addAll(subTablaRespaldo.buscar(getColumna(0) + " = " + id));
		conf.impresionesProperty().get().addAll(subTablaImp.buscar(getColumna(0) + " = " + id));
		
		conf.funcionesProperty().get().addAll(Funcion.parse(subTablaFunciones.buscar(getColumna(0) + " = " + id)));
		conf.ipProperty().get().addAll(IP.parse(subTablaIP.buscar(getColumna(0) + " = " + id)));
		conf.puertosProperty().get().addAll(Puerto.parse(subTablaPuertos.buscar(getColumna(0) + " = " + id)));
		
		conf.usuariosProperty().get().addAll(subTablaUsuarios.buscar(getColumna(0) + " = " + id));
		
		conf.conexionesProperty().get().addAll(Conexion.parse(subTablaConexion.buscar(getColumna(0) + " = " + id)));
		
		conf.problemasProperty().get().addAll(subTablaProblemas.buscar(getColumna(0) + " = " + id));
		
		
		
		
		return conf;
		
	}

	@Override
	public String registrotoString(Config registro) {
		//ConfP_ID 	Serial 	Prod_ID 	Nombre 	Versión 	Modo 	Observaciones 
		return registro.getId()+",'"+registro.serialProperty().get()+
				"','"+registro.productoProperty().get().getId()+
				"','"+registro.nombreProperty().get()+
				"','"+registro.versionProperty().get()+
				"','"+registro.modoProperty().get()+
				"','"+registro.observacionesProperty().get()+
				"'";
	}

}
