package com.view;


import com.model.IP;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;

public class ipABMController extends ImplSubConfigABMController<IP> {

	@FXML
	private TableColumn<IP, String> ipColumn;
	@FXML
	private TableColumn<IP, Integer> maskColumn;
	@FXML
	private TableColumn<IP, String> gateColumn;
	@FXML
	private TableColumn<IP, String> dns1Column;
	@FXML
	private TableColumn<IP, String> dns2Column;
	@FXML
	private TableColumn<IP, String> macColumn;
	
	
	
	
	@Override
	public void handleNuevo() {
		showItemDetails(new IP());
		handleModificar();
	}

	@FXML
	public void initialize() {
		cargarTabla();
		ipColumn.setCellValueFactory(cell -> cell.getValue().getIp());
		maskColumn.setCellValueFactory(cell -> cell.getValue().getMask());
		gateColumn.setCellValueFactory(cell -> cell.getValue().getGate());
		dns1Column.setCellValueFactory(cell -> cell.getValue().getDns2());
		dns2Column.setCellValueFactory(cell -> cell.getValue().getDns1());
		macColumn.setCellValueFactory(cell -> cell.getValue().getMac());
		
		getColumnasClass().put(ipColumn, String.class);
		getColumnasClass().put(maskColumn, Integer.class);
		getColumnasClass().put(gateColumn, String.class);
		getColumnasClass().put(dns1Column, String.class);
		getColumnasClass().put(dns2Column, String.class);
		getColumnasClass().put(macColumn, String.class);
		cargarGrid();
		cargarColumnsSelector();
		
	}
	
	

}
