package com.view;

import java.sql.SQLException;

import com.db.DBTabla;
import com.db.TablaIndexado;
import com.tool.Listable;
import com.tool.failSaveException;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;

public class ListableABMController<T extends Listable> extends ABMController<T> {

	@Override
	public void handleNuevo() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void guardarDB() throws failSaveException, SQLException, NullPointerException {
//		if (getMostrando().getId() == null) {
			TablaIndexado<T> tabla = (TablaIndexado<T>) getMostrando().getTabla();
			Integer id = tabla.agregarRegistro(getMostrando());
			getMostrando().setId(id);
//		}

	}
	public void handleBorrar() {
		Alert alerta = new Alert(AlertType.CONFIRMATION);
		alerta.setHeaderText("Vas a borrar el elemento:");
		alerta.setTitle("¿Estas Seguro?");
		alerta.setContentText(getMostrando().getStringId());
		if (alerta.showAndWait().get().getButtonData()==ButtonData.OK_DONE) {
			TablaIndexado<T> tabla = (TablaIndexado<T>) getMostrando().getTabla();
			try {
				tabla.borrar(tabla.getColumna(0)+"="+getMostrando().getId());
				Alert good = new Alert(AlertType.WARNING);
				good.setHeaderText("Se eliminó");
				good.setTitle("CORRECTO");
				good.setContentText(getMostrando().getStringId());
				getMostrando().setId(null);
				showItemDetails(getMostrando());
				getMostrando().salirLista();
				table.getItems().remove(getMostrando());
				good.showAndWait();
			} catch (NullPointerException | SQLException e) {
				Alert bad = new Alert(AlertType.WARNING);
				bad.setHeaderText("No se pudo borrar");
				bad.setTitle("FALLO");
				bad.setContentText(e.getMessage());
				bad.showAndWait();
			}
		}
	}
	

}
