package com.view;

import java.io.IOException;

import com.model.Config;
import com.model.Problema;
import com.model.Producto;
import com.model.Puerto;
import com.tool.Calculo;
import com.tool.PrinterList;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;

public class ConfiguracionABMController extends ListableABMController<Config> {

	@FXML
	private TableColumn<Config, Integer> idColumn;
	@FXML
	private TableColumn<Config, String> serialColumn;
	@FXML
	private TableColumn<Config, Producto> productoColumn;
	@FXML
	private TableColumn<Config, String> nombreColumn;
	@FXML
	private TableColumn<Config, String> versionColumn;
	@FXML
	private TableColumn<Config, String> modoColumn;
	@FXML
	private TableColumn<Config, String> observacionesColumn;
	@FXML
	private TableColumn<Config, PrinterList<Puerto>> puertosColumn;
	@FXML
	private TableColumn<Config, PrinterList<String>> impColumn;
	@FXML
	private TableColumn<Config, PrinterList<String>> respaldosColumn;

	@FXML
	private SplitPane spliter;

	@FXML
	public void initialize() {
		cargarTabla();
		idColumn.setCellValueFactory(cell -> cell.getValue().idProperty());
		serialColumn.setCellValueFactory(cell -> cell.getValue().serialProperty());
		productoColumn.setCellValueFactory(cell -> cell.getValue().productoProperty());
		nombreColumn.setCellValueFactory(cell -> cell.getValue().nombreProperty());
		versionColumn.setCellValueFactory(cell -> cell.getValue().versionProperty());
		modoColumn.setCellValueFactory(cell -> cell.getValue().modoProperty());
		observacionesColumn.setCellValueFactory(cell -> cell.getValue().observacionesProperty());
		puertosColumn.setCellValueFactory(cell -> cell.getValue().puertosProperty());
		impColumn.setCellValueFactory(cell -> cell.getValue().impresionesProperty());
		respaldosColumn.setCellValueFactory(cell -> cell.getValue().respaldosProperty());

		getColumnasClass().put(idColumn, Calculo.class);
		getColumnasClass().put(serialColumn, String.class);
		getColumnasClass().put(productoColumn, Producto.class);
		getColumnasClass().put(nombreColumn, String.class);
		getColumnasClass().put(versionColumn, String.class);
		getColumnasClass().put(modoColumn, String.class);
		getColumnasClass().put(observacionesColumn, String.class);
		getColumnasClass().put(puertosColumn, PrinterList.class);
		getColumnasClass().put(impColumn, PrinterList.class);
		getColumnasClass().put(respaldosColumn, PrinterList.class);

		cargarGrid();
		cargarColumnsSelector();

	}

	@FXML
	private void abrirConexiones() {
		
		if (getMostrando() != null)
			if (getMostrando().problemasProperty().get() != null) {
				ConexionesABMController controller = (ConexionesABMController) controller("Conexion");
				controller.setLista(getMostrando().conexionesProperty());
				table.setDisable(true);
				controller.getCerrar().setOnAction(e -> {
					table.setDisable(false);
					limitarSpliter(2);
				});
			}
		
	}

	private SubConfigABMController controller(String name) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(ConfiguracionABMController.class.getResource(name + "ABM.fxml"));
		try {
			loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SubConfigABMController controller = loader.getController();
		limitarSpliter(2);
		spliter.getItems().add(controller.getPane());

		return controller;
	}

	private void limitarSpliter(int limit) {
		while (spliter.getItems().size() > limit)
			spliter.getItems().remove(limit);
	}

	@FXML
	private void abrirIP() {
		if (getMostrando() != null)
			if (getMostrando().problemasProperty().get() != null) {
				ipABMController controller = (ipABMController) controller("ip");
				controller.setLista(getMostrando().ipProperty());
				table.setDisable(true);
				controller.getCerrar().setOnAction(e -> {
					table.setDisable(false);
					limitarSpliter(2);
				});
			}
	}

	@FXML
	private void abrirFunciones() {
		if (getMostrando() != null)
			if (getMostrando().funcionesProperty().get() != null) {
				FuncionesABMController controller = (FuncionesABMController) controller("Funciones");
				controller.setLista(getMostrando().funcionesProperty());
				table.setDisable(true);
				controller.getCerrar().setOnAction(e -> {
					table.setDisable(false);
					limitarSpliter(2);
				});
			}
	}
	
	@FXML
	private void abrirUsuarios() {
		if (getMostrando() != null)
			if (getMostrando().funcionesProperty().get() != null) {
				System.out.println("abriendo");
				UsuariosABMController controller = (UsuariosABMController) controller("Usuarios");
				System.out.println(1);
				controller.setLista(getMostrando().usuariosProperty());
				table.setDisable(true);
				System.out.println(2);
				controller.getCerrar().setOnAction(e -> {
					table.setDisable(false);
					limitarSpliter(2);
				});
				System.out.println("ya esta");
			}
	}

	@FXML
	private void abrirProblemas() {
		if (getMostrando() != null)
			if (getMostrando().problemasProperty().get() != null) {
				ProblemasABMController controller = (ProblemasABMController) controller("Problemas");
				controller.setLista(getMostrando().problemasProperty());
				table.setDisable(true);
				controller.getCerrar().setOnAction(e -> {
					table.setDisable(false);
					limitarSpliter(2);
				});
			}
	}

	@FXML
	private void abrirProducto() {
		if (getMostrando() != null)
			if (getMostrando().productoProperty().get() != null) {
				ProductosABMController controller = (ProductosABMController) controller("Productos");
				controller.showItemDetails(getMostrando().productoProperty().get());
				controller.hideBotonAgregar();
			} else {
				Alert alerta = new Alert(AlertType.WARNING);
				alerta.setHeaderText("No hay producto en esta configuracion");
				alerta.showAndWait();
			}
	}

	@FXML
	public void handleNuevo() {
		showItemDetails(new Config(null));
		handleModificar();
	}
}
