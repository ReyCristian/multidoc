package com.view;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.db.DBTabla;
import com.tool.ABMio;
import com.tool.CalculatedField;
import com.tool.StringField;
import com.tool.failSaveException;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;

public abstract class ABMController<T> {
	
	
	
	@FXML
	protected TableView<T> table;

	@FXML
	protected GridPane grid;

	@FXML
	protected Button modificar;

	private Map<TableColumn<?, ?>, Class<?>> columnasClass;

	private T mostrando;
	private T temporal;

	protected String camposPrincipales() {
		return "falta especificar";
	}

	public ABMController() {
		super();
		columnasClass = new HashMap<TableColumn<?, ?>, Class<?>>();
	}

	protected void cargarTabla() {
		table.setItems(FXCollections.observableArrayList());
		table.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> showItemDetails(newValue));
	}

	protected void cargarGrid() {
		int i = 0;
		for (TableColumn<?, ?> columna : table.getColumns()) {
			Label label = new Label(columna.getText());
			GridPane.setValignment(label, VPos.CENTER);
			StringField text = new StringField();
			text.setEditable(false);
			GridPane.setConstraints(label, 0, i);
			GridPane.setConstraints(text, 1, i++);
			grid.getRowConstraints().add(new RowConstraints(44d));
			grid.getChildren().addAll(label, text);
		}
		if (modificar != null)
			modificar.setOnAction(e -> handleModificar());

	}

	protected void showItemDetails(T item) {
		mostrando = item;
		if (item != null) {
			for (int i = 0; i < table.getColumns().size(); i++) {
				Object atributo = table.getColumns().get(i).getCellData(item);
				Node atributoField = ABMio.CrearABMNode(atributo, getColumnasClass(i));
				gridReplace(atributoField, i, 1);
				gridResize(i, ((ABMio) atributoField).getSize());

			}
		}
	}

	private void gridResize(int i, double heigth) {
		grid.getRowConstraints().remove(i);
		RowConstraints size = new RowConstraints(heigth);
		size.setMaxHeight(200d);
		size.setMinHeight(44d);
		grid.getRowConstraints().add(i, size);

	}

	protected void gridReplace(Node nuevo, int fila, int columna) {
		GridPane.setConstraints(nuevo, columna, fila);
		grid.getChildren().remove(fila * grid.getColumnCount() + columna);
		grid.getChildren().add(fila * grid.getColumnCount() + columna, nuevo);
	}

	protected void cargarColumnsSelector() {
		table.setOnContextMenuRequested(e -> {
			final ContextMenu contextMenu = new ContextMenu();

			for (TableColumn<?, ?> columna : table.getColumns()) {
				CheckMenuItem item = new CheckMenuItem(columna.getText());
				item.setSelected(columna.visibleProperty().get());
				item.setOnAction(c -> {
					columna.setVisible(!columna.visibleProperty().get());
				});
				contextMenu.getItems().add(item);
			}
			table.setContextMenu(contextMenu);
		});
	}

	protected void setEditableFields(boolean editable) {
		for (int i = 0; i < table.getColumns().size(); i++) {
			ABMio item = (ABMio) grid.getChildren().get(i * 2 + 1);
			item.setEnable(editable);
		}
	}

	@FXML
	public void handleModificar() {
		if (mostrando != null) {
			table.setDisable(true);
			setEditableFields(true);
			modificar.setText("Guardar");
			modificar.setOnAction(e -> handleGuardar());
			showItemDetails(mostrando);
			setEditableFields(true);
			modificar.getParent().getChildrenUnmodifiable().forEach(e->e.setVisible(false));
			modificar.setVisible(true);
		} else {
			Alert alerta = new Alert(AlertType.WARNING);
			alerta.setHeaderText("No seleccionaste nada");
			alerta.showAndWait();
		}
	}

	@FXML
	public void handleGuardar() {
		try {
			descargar(mostrando);
			guardarDB();
			if (table.getItems().indexOf(mostrando) < 0)
				table.getItems().add(mostrando);
			setEditableFields(false);
			table.setDisable(false);
			modificar.setText("Modificar");
			modificar.setOnAction(e -> handleModificar());
			showItemDetails(mostrando);
			modificar.getParent().getChildrenUnmodifiable().forEach(e->e.setVisible(true));
		} catch (NullPointerException e) {
			Alert alerta = new Alert(AlertType.WARNING);
			alerta.setHeaderText("Tiene que llenar los campos  principales");
			alerta.setTitle("Campo vacio o erroneo");
			alerta.setContentText(camposPrincipales());
			e.printStackTrace();
			alerta.showAndWait();
		} catch (NumberFormatException e) {
			Alert alerta = new Alert(AlertType.WARNING);
			alerta.setHeaderText("Tiene q meter un numero");
			alerta.setTitle("Numero incorrecto");
			alerta.setContentText(e.getLocalizedMessage());
			e.printStackTrace();
			alerta.showAndWait();
		} catch (failSaveException | SQLException e) {
			Alert alerta = new Alert(AlertType.WARNING);
			alerta.setHeaderText("Respuesta de la base de datos:");
			alerta.setTitle("No se puede Guardar");
			alerta.setContentText(e.getLocalizedMessage());
			e.printStackTrace();
			alerta.showAndWait();
		}

	}

	protected abstract void guardarDB() throws failSaveException, SQLException;

	private void descargar(T mostrando) {
		for (int i = 0; i < table.getColumns().size(); i++) {
			ObservableValue<?> propiedad = table.getColumns().get(i).getCellObservableValue(mostrando);
			ABMio campo = (ABMio) grid.getChildren().get(i * 2 + 1);

			if (campo.getClass() == CalculatedField.class) 
				((CalculatedField) campo).recalcular();
			if (propiedad.getClass() == SimpleStringProperty.class)
				((SimpleStringProperty) propiedad).set(campo.getValor().toString());
			else
				((ObjectProperty<Object>) propiedad).set(campo.getValor());


		}

	}

	public T getMostrando() {
		return mostrando;
	}

	public Map<TableColumn<?, ?>, Class<?>> getColumnasClass() {
		return columnasClass;
	}

	public Class<?> getColumnasClass(TableColumn<?, ?> key) {
		return columnasClass.getOrDefault(key, null);
	}

	public Class<?> getColumnasClass(int i) {
		return getColumnasClass(table.getColumns().get(i));
	}

	@FXML
	public abstract void handleNuevo();

	public void addItems(List<T> items) {
		table.getItems().addAll(items);
	}

	void cargar(DBTabla<T> tabla) {
		{
			if (tabla != null)
				try {
					List<T> lista = tabla.leer();
					this.addItems(lista);
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
	}

}
