package com.view;

import com.tool.PrinterList;

import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;

public interface SubConfigABMController {
	public Pane getPane();
	
	public Button getCerrar();
	
}
