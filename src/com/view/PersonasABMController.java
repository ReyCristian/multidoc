package com.view;

import com.model.Persona;
import com.tool.Calculo;
import com.tool.PrinterList;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;

public abstract class PersonasABMController<P extends Persona> extends ListableABMController<P> {

	@FXML
	protected TableColumn<P, Integer> idColumn;
	@FXML
	protected TableColumn<P, String> nameColumn;
	@FXML
	protected TableColumn<P, PrinterList<Integer>> telColum;
	@FXML
	protected TableColumn<P, PrinterList<String>> emailColum;

	@FXML
	protected void initialize() {
		cargarTabla();
		idColumn.setCellValueFactory(cell -> cell.getValue().idProperty());
		nameColumn.setCellValueFactory(cell -> cell.getValue().nombreProperty());
		telColum.setCellValueFactory(cell -> cell.getValue().telefonoProperty());
		emailColum.setCellValueFactory(cell -> cell.getValue().emailProperty());

		getColumnasClass().put(idColumn, Calculo.class);
		getColumnasClass().put(nameColumn, String.class);
		getColumnasClass().put(telColum, PrinterList.class);
		getColumnasClass().put(emailColum, PrinterList.class);

		cargarGrid();
		cargarColumnsSelector();
	}

}
