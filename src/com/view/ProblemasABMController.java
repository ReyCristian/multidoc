package com.view;

import com.model.Problema;
import com.model.Solucion;
import com.tool.Calculo;
import com.tool.PrinterList;


import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;

public class ProblemasABMController extends ImplSubConfigABMController<Problema>{

	@FXML
	private TableColumn<Problema, Integer> idColumn;
	@FXML
	private TableColumn<Problema, String> problemaColumn;
	@FXML
	private TableColumn<Problema, PrinterList<Solucion>> solucionesColumn;

	
	
	@FXML
	private void initialize() {
		cargarTabla();
		idColumn.setCellValueFactory(cell -> cell.getValue().idProperty());
		problemaColumn.setCellValueFactory(cell -> cell.getValue().get());
		solucionesColumn.setCellValueFactory(cell -> cell.getValue().solucionesProperty());
		
		getColumnasClass().put(idColumn, Calculo.class);
		getColumnasClass().put(problemaColumn, String.class);
		getColumnasClass().put(solucionesColumn, Solucion.class);
		cargarGrid();
		cargarColumnsSelector();
		
	}

	@Override
	public void handleNuevo() {
		showItemDetails(new Problema(null));
		handleModificar();
		
	}

	
}
