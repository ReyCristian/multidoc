package com.view;


import java.sql.SQLException;

import com.db.TablaIndexado;
import com.tool.PrinterList;
import com.tool.failSaveException;

import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.Pane;

public abstract class ImplSubConfigABMController<E> extends ABMController<E> implements SubConfigABMController{
	
	@FXML
	private Pane pane;
	@FXML
	private Button cerrar;
	
	@Override
	public Pane getPane() {
		// TODO Auto-generated method stub
		return pane;
	}

	
	public void setLista(SimpleObjectProperty<PrinterList<E>> lista){
		
		lista.get().forEach(e->table.getItems().add(e));
		table.getItems().addListener(new ListChangeListener<E>() {

			@Override
			public void onChanged(Change<? extends E> arg0) {
			while(arg0.next()) {
				arg0.getAddedSubList().forEach(e->lista.get().add(e));
			}
			}
		});
	}
	
	public Button getCerrar(){
		return cerrar;
	}
	
	@Override
	protected void guardarDB() throws failSaveException, SQLException {
		// TODO Auto-generated method stub
		
	}
	
	
	

}
