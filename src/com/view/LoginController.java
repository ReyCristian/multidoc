package com.view;

import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.Alert.AlertType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DataBase;
import com.db.DataBaseController;
import com.launcher.Main;
import com.model.entidadesEnum;

import javafx.fxml.FXML;

public class LoginController {

	@FXML
	private TextField user;

	@FXML
	private PasswordField pass;

	private Main main;
	
	@FXML
	private ImageView image;

	@FXML
	private void initialize() {

	}

	@FXML
	private void handleOK() {

		String sql = "SELECT `acceso` FROM `logeo_sistema` WHERE `usuario`='" + user.getText() + "' AND `pass`='"
				+ pass.getText() + "'";
		System.out.println(sql);
		Statement stmt;
		try {
			stmt = (new DataBase()).tryConectarMySQL().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next())
				main.launchPrincipal(rs.getInt(1));
			else {
				Alert alerta = new Alert(AlertType.WARNING);
				alerta.setTitle("No se pudo iniciar sesion");
				alerta.setContentText("Nombre de usuario o contraseña incorrecta");
				alerta.showAndWait();
			}
		} catch (Exception e) {
			Alert alerta = new Alert(AlertType.WARNING);
			alerta.setTitle("No se pudo iniciar sesion");
			alerta.setContentText(e.getMessage());
			alerta.showAndWait();
		}

	}

	@FXML
	private void handleCancel() {
		main.closeLogin();

	}

	public Main getMain() {
		return main;
	}

	public void setMain(Main main) {
		this.main = main;
	}

	public Image getLogo() {
		System.out.println(image);
		return image.getImage();
	}

}
