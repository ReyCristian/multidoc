package com.view;


import com.model.Config;
import com.model.Empleado;
import com.model.Entrada;
import com.model.Usuario;
import com.tool.Calculo;
import com.tool.PrinterDate;
import com.tool.PrinterList;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;

public class UsuariosABMController extends ImplSubConfigABMController<Usuario> {

//	Entrada_ID	Fecha_Entrada	ConfP_ID	Motivo	Empleado	Almacen

	@FXML
	protected TableColumn<Usuario, String> userColumn;
	@FXML
	protected TableColumn<Usuario, String> passColumn;
	@FXML
	protected TableColumn<Usuario, String> sectorColumn;
	@FXML
	protected TableColumn<Usuario, String> puestoColumn;
	@FXML
	protected TableColumn<Usuario, PrinterList<String>> permisosColumn;

	@FXML
	protected void initialize() {
		cargarTabla();

		userColumn.setCellValueFactory(cell -> cell.getValue().getNombreUsuario());
		passColumn.setCellValueFactory(cell -> cell.getValue().getPass());
		sectorColumn.setCellValueFactory(cell -> cell.getValue().getSector());
		puestoColumn.setCellValueFactory(cell -> cell.getValue().getPuesto());
		permisosColumn.setCellValueFactory(cell -> cell.getValue().getPermisos());
		
		
		getColumnasClass().put(userColumn, String.class);
		getColumnasClass().put(passColumn, String.class);
		getColumnasClass().put(sectorColumn, String.class);
		getColumnasClass().put(puestoColumn, String.class);
		getColumnasClass().put(permisosColumn, PrinterList.class);
		
		cargarGrid();
		cargarColumnsSelector();

	}

	@Override
	public void handleNuevo() {
		showItemDetails(new Usuario(0));
		handleModificar();
		
	}

}
