package com.view;

import com.model.Cliente;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;

public class ClientesABMController extends PersonasABMController<Cliente> {

	@FXML
	private TableColumn<Cliente, String> addressColum;

	@FXML
	protected void initialize() {
		super.initialize();
		addressColum.setCellValueFactory(cell -> cell.getValue().direccionProperty());

		getColumnasClass().put(addressColum, String.class);
	}

	@FXML
	public void handleNuevo() {
		showItemDetails(new Cliente(null));
		handleModificar();

	}
}
