package com.view;

import com.model.Empleado;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;

public class EmpleadosABMController extends PersonasABMController<Empleado> {

	@FXML
	protected TableColumn<Empleado, Integer> DNIColumn;
	@FXML
	protected TableColumn<Empleado, String> apellidoColumn;

	@FXML
	protected void initialize() {
		super.initialize();
		apellidoColumn.setCellValueFactory(cell -> cell.getValue().apellidoProperty());
		DNIColumn.setCellValueFactory(cell -> cell.getValue().DNIProperty());
		
		getColumnasClass().put(DNIColumn, Integer.class);
		getColumnasClass().put(apellidoColumn, String.class);
		
	}

	@Override
	public void handleNuevo() {
		showItemDetails(new Empleado(null));
		handleModificar();
		
	}
}
