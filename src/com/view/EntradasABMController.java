package com.view;

import com.model.Config;
import com.model.Empleado;
import com.model.Entrada;
import com.tool.Calculo;
import com.tool.PrinterDate;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;

public class EntradasABMController extends ListableABMController<Entrada> {

//	Entrada_ID	Fecha_Entrada	ConfP_ID	Motivo	Empleado	Almacen

	@FXML
	protected TableColumn<Entrada, Integer> idColumn;
	@FXML
	protected TableColumn<Entrada, PrinterDate> fechaColumn;
	@FXML
	protected TableColumn<Entrada, Config> confP_IDColumn;
	@FXML
	protected TableColumn<Entrada, String> motivoColumn;
	@FXML
	protected TableColumn<Entrada, Empleado> empleadoColumn;
	@FXML
	protected TableColumn<Entrada, String> almacenColumn;
	@Override
	protected String camposPrincipales() {
		return "Equipo y Empleados";
	}

	@FXML
	protected void initialize() {
		cargarTabla();

		idColumn.setCellValueFactory(cell -> cell.getValue().idProperty());
		fechaColumn.setCellValueFactory(cell -> cell.getValue().fechaProperty());
		confP_IDColumn.setCellValueFactory(cell -> cell.getValue().confP_IDProperty());
		motivoColumn.setCellValueFactory(cell -> cell.getValue().motivoProperty());
		empleadoColumn.setCellValueFactory(cell -> cell.getValue().empleadoProperty());
		almacenColumn.setCellValueFactory(cell -> cell.getValue().almacenProperty());

		getColumnasClass().put(idColumn, Calculo.class);
		getColumnasClass().put(fechaColumn, PrinterDate.class);
		getColumnasClass().put(confP_IDColumn, Config.class);
		getColumnasClass().put(motivoColumn, String.class);
		getColumnasClass().put(empleadoColumn, Empleado.class);
		getColumnasClass().put(almacenColumn, String.class);

		cargarGrid();
		cargarColumnsSelector();

	}

	@Override
	public void handleNuevo() {
		showItemDetails(new Entrada(null));
		handleModificar();

	}

}
