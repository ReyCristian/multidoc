package com.view;

import com.model.Cliente;
import com.model.Config;
import com.model.Proyecto;
import com.tool.Calculo;
import com.tool.PrinterList;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;

public class ProyectosABMController extends ListableABMController<Proyecto> {

	@FXML
	protected TableColumn<Proyecto, Integer> idColumn;
	@FXML
	protected TableColumn<Proyecto, String> nameColumn;
	
	@FXML
	protected TableColumn<Proyecto, Cliente> clienteColumn;
	@FXML
	protected TableColumn<Proyecto,  PrinterList<String>> observacionesColumn;

	@FXML
	protected TableColumn<Proyecto,  PrinterList<String>> planosColumn;

	@FXML
	protected TableColumn<Proyecto,  PrinterList<String>> informesColumn;
	

	@FXML
	private void initialize() {
		cargarTabla();
		idColumn.setCellValueFactory(cell -> cell.getValue().idProperty());
		nameColumn.setCellValueFactory(cell -> cell.getValue().nombreProperty());
		clienteColumn.setCellValueFactory(cell -> cell.getValue().clienteProperty());
		observacionesColumn.setCellValueFactory(cell -> cell.getValue().observacionesProperty());
		planosColumn.setCellValueFactory(cell -> cell.getValue().planosProperty());
		informesColumn.setCellValueFactory(cell -> cell.getValue().informesProperty());
		
		getColumnasClass().put(idColumn, Calculo.class);
		getColumnasClass().put(nameColumn, String.class);
		getColumnasClass().put(clienteColumn, Cliente.class);
		getColumnasClass().put(observacionesColumn, PrinterList.class);
		getColumnasClass().put(planosColumn, PrinterList.class);
		getColumnasClass().put(informesColumn, PrinterList.class);
		
		
		
		cargarGrid();
		cargarColumnsSelector();
		
		Proyecto proyecto = new Proyecto(51);
		proyecto.informesProperty().get().add("asd1");
		proyecto.informesProperty().get().add("asd2");
		proyecto.informesProperty().get().add("asd3");
		proyecto.informesProperty().get().add("asd1");
		proyecto.informesProperty().get().add("asd2");
		proyecto.informesProperty().get().add("asd3");
		proyecto.informesProperty().get().add("asd1");
		proyecto.informesProperty().get().add("asd2");
		proyecto.informesProperty().get().add("asd3");
		proyecto.informesProperty().get().add("asd1");
		proyecto.informesProperty().get().add("asd2");
		proyecto.informesProperty().get().add("asd3");
		table.getItems().add(proyecto);
		
		
	}


	@Override
	public void handleNuevo() {
		showItemDetails(new Proyecto(null));
		handleModificar();
		
	}
	
	
}
