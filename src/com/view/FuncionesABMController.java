package com.view;

import com.model.Funcion;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;

public class FuncionesABMController extends ImplSubConfigABMController<Funcion> {

	@FXML
	private TableColumn<Funcion, String> atajoColum;
	@FXML
	private TableColumn<Funcion, String> funcionColumn;

	@FXML
	private void initialize() {
		cargarTabla();
		cargarTabla();
		
		atajoColum.setCellValueFactory(cell -> cell.getValue().getAtajo());
		funcionColumn.setCellValueFactory(cell -> cell.getValue().getFuncion());
		
		getColumnasClass().put(atajoColum, String.class);
		getColumnasClass().put(funcionColumn, String.class);
		
		cargarGrid();
		cargarColumnsSelector();

	}

	@FXML
	public void handleNuevo() {
		showItemDetails(new Funcion());
		handleModificar();
	}
}
