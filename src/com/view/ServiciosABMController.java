package com.view;

import com.model.Cliente;
import com.model.Config;
import com.model.Empleado;
import com.model.Insumo;
import com.model.Producto;
import com.model.Proyecto;
import com.model.Servicio;
import com.tool.Calculo;
import com.tool.PrinterDate;
import com.tool.PrinterList;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;

public class ServiciosABMController extends ListableABMController<Servicio>{

	@FXML
	private TableColumn<Servicio,Integer> idColumn;
	@FXML
	private TableColumn<Servicio,PrinterDate> fechaColumn;
	@FXML
	private TableColumn<Servicio,Cliente> clienteColumn;
	@FXML
	private TableColumn<Servicio,Proyecto> proyectoColumn;
	
	@FXML
	private TableColumn<Servicio,String> responsableColumn;
	@FXML
	private TableColumn<Servicio,String> vehiculoColumn;
	@FXML
	private TableColumn<Servicio,String> observacionesColumn;
	@FXML
	private TableColumn<Servicio,PrinterList<Config>> equiposColumn;
	
	
	@FXML
	private TableColumn<Servicio,PrinterList<Empleado>> empleadoColumn;
	@FXML
	private TableColumn<Servicio,PrinterList<String>> planosColumn;
	@FXML
	private TableColumn<Servicio,PrinterList<Insumo>> materialesColumn;
	@FXML
	protected void initialize() {
		cargarTabla();
		idColumn.setCellValueFactory(cell -> cell.getValue().idProperty());
		fechaColumn.setCellValueFactory(cell -> cell.getValue().fechaProperty());
		clienteColumn.setCellValueFactory(cell -> cell.getValue().clienteProperty());
		proyectoColumn.setCellValueFactory(cell -> cell.getValue().proyectoProperty());
		responsableColumn.setCellValueFactory(cell -> cell.getValue().responsableProperty());
		vehiculoColumn.setCellValueFactory(cell -> cell.getValue().vehiculoProperty());
		observacionesColumn.setCellValueFactory(cell -> cell.getValue().observacionesProperty());
		equiposColumn.setCellValueFactory(cell -> cell.getValue().equiposProperty());
		empleadoColumn.setCellValueFactory(cell -> cell.getValue().empleadoProperty());
		planosColumn.setCellValueFactory(cell -> cell.getValue().planosProperty());
		materialesColumn.setCellValueFactory(cell -> cell.getValue().materialesProperty());
		cargarGrid();
		cargarColumnsSelector();
		
		getColumnasClass().put(idColumn, Calculo.class);
		getColumnasClass().put(fechaColumn, PrinterDate.class);
		getColumnasClass().put(clienteColumn, Cliente.class);
		getColumnasClass().put(proyectoColumn, Proyecto.class);
		getColumnasClass().put(responsableColumn, String.class);
		getColumnasClass().put(vehiculoColumn, String.class);
		getColumnasClass().put(observacionesColumn, String.class);
		getColumnasClass().put(equiposColumn, PrinterList.class);
		getColumnasClass().put(empleadoColumn, PrinterList.class);
		getColumnasClass().put(planosColumn, PrinterList.class);
		getColumnasClass().put(materialesColumn, PrinterList.class);
		
		
		Servicio s = new Servicio(1);
		s.clienteProperty().set(new Cliente(1));
		table.getItems().add(s);
		s = new Servicio(2);
		s.fechaProperty().set(new PrinterDate());
		table.getItems().add(s);
	}
	@Override
	public void handleNuevo() {
		showItemDetails(new Servicio(null));
		handleModificar();
		
	}
	
}
