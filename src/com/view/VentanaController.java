package com.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.db.DataBaseController;
import com.model.entidadesEnum;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

public class VentanaController {

	@FXML
	private TabPane tabPane;

	@FXML
	private void initialize() {
		
//		TextField t2 = new TextField("Inbox");
//		t2.setDisable(true);
//		TreeItem<TextField> rootItem = new TreeItem<TextField>(t2);
//		rootItem.setExpanded(true);
//		for (int i = 1; i < 6; i++) {
//			TextField t = new TextField("Message" + i);
//			TreeItem<TextField> item = new TreeItem<TextField>(t);
//			rootItem.getChildren().add(item);
//		}
//		TreeView<TextField> tree = new TreeView<TextField>(rootItem);
//		Pane root = new Pane();
//		root.getChildren().add(tree);
//		agregarTab("welcome", root);

	}

	public Tab agregarTab(String name, Pane root) {
		
		
		Tab tab = new Tab();
		tab.setText(name);

		tab.setContent(root);
		tabPane.getTabs().add(tab);
		
		
		return tab;
	}

	@FXML
	private void handleListProduct() {
		handleNewTabABM(entidadesEnum.PRODUCTOS);

	}

	@FXML
	private void handleClient() {
		handleNewTabABM(entidadesEnum.CLIENTES);
	}

	@FXML
	private void handleEmployee() {
		handleNewTabABM(entidadesEnum.EMPLEADOS);

	}

	@FXML
	private void handleProyect() {
		handleNewTabABM(entidadesEnum.PROYECTOS);

	}

	@FXML
	private void handleEntrada() {
		handleNewTabABM(entidadesEnum.ENTRADAS);

	}

	@FXML
	private void handleServicio() {
		handleNewTabABM(entidadesEnum.SERVICIOS);

	}

	@FXML
	private void handleConfig() {
		handleNewTabABM(entidadesEnum.CONFIGURACION);

	}

	private ABMController<?> handleNewTabABM(entidadesEnum entidad) {
		try {
			System.out.println("abriendo " + entidad);
			AnchorPane abm;
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(ABMController.class.getResource(entidad.getFxml() + "ABM.fxml"));

			abm = (AnchorPane) loader.load();

			agregarTab(entidad.getTabName(), abm);
			if (loader != null) {
				ABMController controller = loader.getController();

				controller.cargar(DataBaseController.instance().getTabla(entidad.getTabla()));

				return controller;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
