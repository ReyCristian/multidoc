package com.view;

import java.sql.SQLException;

import com.db.TablaIndexado;
import com.model.Conexion;
import com.model.Config;
import com.model.Producto;
import com.model.Puerto;
import com.tool.ABMio;
import com.tool.Calculo;
import com.tool.PrinterList;
import com.tool.failSaveException;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;

public class ConexionesABMController extends ImplSubConfigABMController<Conexion> {

	private Config principal;
	@FXML
	private TableColumn<Conexion, String> enlaceColumn;
	@FXML
	private TableColumn<Conexion, Integer> idColumn;
	@FXML
	private TableColumn<Conexion, String> serialColumn;
	@FXML
	private TableColumn<Conexion, Producto> productoColumn;
	@FXML
	private TableColumn<Conexion, String> nombreColumn;
	@FXML
	private TableColumn<Conexion, String> versionColumn;
	@FXML
	private TableColumn<Conexion, String> modoColumn;
	@FXML
	private TableColumn<Conexion, String> observacionesColumn;
	@FXML
	private TableColumn<Conexion, PrinterList<Puerto>> puertosColumn;
	@FXML
	private TableColumn<Conexion, PrinterList<String>> impColumn;
	@FXML
	private TableColumn<Conexion, PrinterList<String>> respaldosColumn;

	@FXML
	private SplitPane spliter;

	@FXML
	public void initialize() {
		cargarTabla();

		enlaceColumn.setCellValueFactory(cell -> cell.getValue().tipoProperty());
		idColumn.setCellValueFactory(cell -> cell.getValue().confP_IDProperty().get().idProperty());
		serialColumn.setCellValueFactory(cell -> cell.getValue().confP_IDProperty().get().serialProperty());
		productoColumn.setCellValueFactory(cell -> cell.getValue().confP_IDProperty().get().productoProperty());
		nombreColumn.setCellValueFactory(cell -> cell.getValue().confP_IDProperty().get().nombreProperty());
		versionColumn.setCellValueFactory(cell -> cell.getValue().confP_IDProperty().get().versionProperty());
		modoColumn.setCellValueFactory(cell -> cell.getValue().confP_IDProperty().get().modoProperty());
		observacionesColumn
				.setCellValueFactory(cell -> cell.getValue().confP_IDProperty().get().observacionesProperty());
		puertosColumn.setCellValueFactory(cell -> cell.getValue().confP_IDProperty().get().puertosProperty());
		impColumn.setCellValueFactory(cell -> cell.getValue().confP_IDProperty().get().impresionesProperty());
		respaldosColumn.setCellValueFactory(cell -> cell.getValue().confP_IDProperty().get().respaldosProperty());

		getColumnasClass().put(enlaceColumn, String.class);
		getColumnasClass().put(idColumn, Calculo.class);
		getColumnasClass().put(serialColumn, String.class);
		getColumnasClass().put(productoColumn, Producto.class);
		getColumnasClass().put(nombreColumn, String.class);
		getColumnasClass().put(versionColumn, String.class);
		getColumnasClass().put(modoColumn, String.class);
		getColumnasClass().put(observacionesColumn, String.class);
		getColumnasClass().put(puertosColumn, PrinterList.class);
		getColumnasClass().put(impColumn, PrinterList.class);
		getColumnasClass().put(respaldosColumn, PrinterList.class);

		cargarGrid();
		cargarColumnsSelector();

	}

	@FXML
	public void handleNuevo() {
		System.out.println(Config.getLista());
		Stage ventana = new Stage();
		ventana.setTitle("Nueva Conexion");

		Node nuevo = ABMio.CrearABMNode(null, Config.class);
		ABMio nuevoABMio = ((ABMio) nuevo);
		nuevoABMio.setEnable(true);
		BorderPane.setAlignment(nuevo, Pos.CENTER);

		Button ok = new Button("OK");
		BorderPane.setAlignment(ok, Pos.BOTTOM_RIGHT);
		ok.setOnAction(e -> {
			Conexion con = new Conexion();
			con.confP_IDProperty().set((Config) nuevoABMio.getValor());
			table.getItems().add(con);
			ventana.close();
		});

		BorderPane root = new BorderPane(nuevo, null, null, (Node) ok, null);

		ventana.setScene(new Scene(root, 300, 100));
		root.getScene().getStylesheets().add(this.getClass().getResource("DarkTheme.css").toExternalForm());
		root.getStyleClass().add("background");
		ventana.showAndWait();
//		showItemDetails(new Conexion());
	}

	@Override
	protected void guardarDB() throws failSaveException, SQLException, NullPointerException {
//		if (getMostrando().getId() == null) {
		TablaIndexado<Config> tabla = (TablaIndexado<Config>) getMostrando().confP_IDProperty().get().getTabla();
		Integer id = tabla.agregarRegistro(getMostrando().confP_IDProperty().get());
//		}

	}

}
