package com.view;

import com.model.Problema;
import com.model.Producto;
import com.tool.Calculo;
import com.tool.PrinterList;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.Pane;

public class ProductosABMController extends ListableABMController<Producto> implements SubConfigABMController{

	@FXML
	private TableColumn<Producto, Integer> idColumn;
	@FXML
	private TableColumn<Producto, String> modelColumn;
	@FXML
	private TableColumn<Producto, String> marca;
	@FXML
	private TableColumn<Producto, String> tipoProducto;
	@FXML
	private TableColumn<Producto, String> caracteristica;

	@FXML
	private TableColumn<Producto,PrinterList<String>> manuales;
	@FXML
	private TableColumn<Producto,PrinterList<String>> observaciones;
	@FXML
	private TableColumn<Producto,PrinterList<Problema>> problemas;
	
	@FXML
	private Pane itemPane;
	@FXML
	private Button agregar;
	@FXML
	private Button cerrar;
	
	
	
	@FXML
	private void initialize() {
		cargarTabla();
		idColumn.setCellValueFactory(cell -> cell.getValue().prodIdProperty());
		modelColumn.setCellValueFactory(cell -> cell.getValue().modeloProperty());
		marca.setCellValueFactory(cell -> cell.getValue().marcaProperty());
		tipoProducto.setCellValueFactory(cell -> cell.getValue().tipoProductoProperty());
		caracteristica.setCellValueFactory(cell -> cell.getValue().caracteristicaProperty());
		manuales.setCellValueFactory(cell -> cell.getValue().manualesProperty());
		observaciones.setCellValueFactory(cell -> cell.getValue().observacionesProperty());
		problemas.setCellValueFactory(cell -> cell.getValue().problemasProperty());
		
		
		getColumnasClass().put(idColumn, Calculo.class);
		getColumnasClass().put(modelColumn, String.class);
		getColumnasClass().put(marca, String.class);
		getColumnasClass().put(tipoProducto, String.class);
		getColumnasClass().put(caracteristica, String.class);
		getColumnasClass().put(manuales, PrinterList.class);
		getColumnasClass().put(observaciones, PrinterList.class);
		getColumnasClass().put(problemas, PrinterList.class);
		
		cargarGrid();
		cargarColumnsSelector();


	}


	@Override
	public void handleNuevo() {
		showItemDetails(new Producto(null));
		handleModificar();
		
	}


	@Override
	public Pane getPane() {
		return itemPane;
	}


	public void hideBotonAgregar() {
		agregar.setVisible(false);
	}
	
	public Button getCerrar(){
		return cerrar;
	}


}
