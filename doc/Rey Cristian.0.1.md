<h1 align="center"> Montaje de Sistemas electronicos a pedido </h1>

---

[**1. Descripción proceso**](#Descripcion)
[**2.Justificación Desarrollo**](#just)
[**3.Definicion del sistema, Objetivos y Alcance**](#Definicion)
[**4.Factibilidad**](#Factibilidad)
[**5.Diagrama Entidad-Relacion**](#Entidad)
[**6.Diseño de Base de Datos**](#Base)
[**7.Diagrama de Casos de Usos**](#Usos)
[**8.Menu del Sistema**](#Menu)
[**9.Desarrollo del Codigo**](#Codigo)
[**10.Conexion a base de datos**](#Conexion)
[**11.Consultas SQL**](#Consultas)
[**12.ABM de una interfaz del sistema**](#ABM)


---
<h2 id=Descripcion> 1. Descripción proceso</h2>

El Proyecto estara orientado a la empresa Multiservicios. El Proceso consiste en el analisis de las necesidades del cliente, el diseño de un sistema: seleccion de productos que mas se adapten a la necesidad, la compra de dichos productos, y el ensamblaje del sistema (instalación) para luego realizar seguimiento y mantenimiento.

El proceso de trabajo de dicha empresa es basicamente el mismo en cada proyecto pero muy diferente en sus detalles por dedicarse a diversos sistemas electrónicos de seguridad, telefonia, redes de datos, etc

Ejemplo: Sistema de VideoVigilancia:
1. Se inspecciona la zona a proteger.
2. Se calcula la cantidad de camaras, y las propiedades requeridas por cada una, las capacidades del dispositivo de grabacion(dvr o servidor).
3. Se compra.
4. Se realiza el cableado, se instala cada camara, dvr, etc.
5. Se configura y documenta la configuracion.
6. Se prueba y evalua.
7. Periodicamente y/o a pedido se realiza mantenimiento, reparaciones, actualizaciones(cambia de lugar una camara, se actuliza el firmware, etc).

<h2 id="Justificacion"> 2. Justificación Desarrollo </h2>

Se ha encontrado que a la hora de hacer reparaciones el sistema de documentacion actual resulta ineficiente:
- Solo cubre configuraciones, pero descuida la dispocion del cableado, caracteristicas de los dispositivos, etc. 
- No existe una estandarización, por lo que cada empleado ha realizado su documentacion de diferente forma, distinto orden y distintas prioridades.

Como consecuencia de esto, los tecnicos deben tomarse el trabajo para analizar la instalación, que con frecuencia requiere desplazarse a otros sectores del establecimiento. El tiempo empleado en esta tarea se traduce en retrasos en los proyectos y perdidas economicas para la empresa que se verian reducidos o hasta eliminados si se dispone correctamente de la documentación.

<h2 id=Definicion> 3.Definicion del sistema, Objetivos y Alcance </h2>

El sistema a desarrollar se centrará en facilitar la creacion de documentacion sobre las diferentes configuraciones usadas en cada uno de los sistemas electronicos instalados por la empresa.  Para ello debera proporcionar:
1. Plantillas para cada tipo de sistemas con los datos propios de cada producto componente, ademas de observaciones hechas por el tecnico
2. Herramientas para agilizar el guardado de datos, de modo que sea comodo de usar en medio de una instalacion. Ej una heramienta que almacene impresiones de pantalla, otra que almacene las respuestas dadas por la shell a un determinado comando
3. Acceso a los datos al buscar por cliente, para conocer la configuracion del mismo, y al buscar por producto por si se necesitan consultar para casos similares en el futuro
4. 



<h2 id=Factibilidad> 4.Factibilidad</h2>
<h2 id=Entidad> 5.Diagrama Entidad-Relacion</h2>
<h2 id=Base> 6.Diseño de Base de Datos</h2>
<h2 id=Usos> 7.Diagrama de Casos de Usos</h2>
<h2 id=Menu> 8.Menu del Sistema</h2>
<h2 id=Codigo> 9.Desarrollo del Codigo</h2>
<h2 id=Conexion> 10.Conexion a base de datos</h2>
<h2 id=Consultas> 11.Consultas SQL</h2>
<h2 id=ABM> 12.ABM de una interfaz del sistema</h2>
