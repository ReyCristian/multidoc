<h1 align="center"> Montaje de Sistemas electrónicos a pedido </h1>

---

[**1. Descripción proceso**](#Descripcion)
[**2.Justificación Desarrollo**](#just)
[**3.Definición del sistema, Objetivos y Alcance**](#Definicion)
[**4.Factibilidad**](#Factibilidad)
[**5.Diagrama Entidad-Relación**](#Entidad)
[**6.Diseño de Base de Datos**](#Base)
[**7.Diagrama de Casos de Usos**](#Usos)
[**8.Menú del Sistema**](#Menu)
[**9.Desarrollo del Código**](#Codigo)
[**10.Conexión a base de datos**](#Conexion)
[**11.Consultas SQL**](#Consultas)
[**12.ABM de una interfaz del sistema**](#ABM)


---
<h2 id=Descripcion> 1. Descripción proceso</h2>

El Proyecto estará orientado a la empresa Multiservicios. El Proceso consiste en el análisis de las necesidades del cliente, el diseño de un sistema: Selección de productos que mas se adapten a la necesidad, la compra de dichos productos, y el ensamblaje del sistema (instalación) para luego realizar seguimiento y mantenimiento.

El proceso de trabajo de dicha empresa es básicamente el mismo en cada proyecto pero muy diferente en sus detalles por dedicarse a diversos sistemas electrónicos de seguridad, telefonía, redes de datos, etc

Ejemplo: Sistema de Vídeo-Vigilancia:
1. Se inspecciona la zona a proteger.
2. Se calcula la cantidad de cámaras, y las propiedades requeridas por cada una, las capacidades del dispositivo de grabación(dvr o servidor).
3. Se compra.
4. Se realiza el cableado, se instala cada cámara, dvr, etc.
5. Se configura y documenta la configuración.
6. Se prueba y evalúa.
7. Periódicamente y/o a pedido se realiza mantenimiento, reparaciones, actualizaciones(cambia de lugar una cámara, se actualiza el firmware, etc).

<h2 id="Justificacion"> 2. Justificación Desarrollo </h2>

Se ha encontrado que a la hora de hacer reparaciones el sistema de documentación actual resulta ineficiente:
- Solo cubre configuraciones, pero descuida la disposición del cableado, características de los dispositivos, etc. 
- No existe una estandarización, por lo que cada empleado ha realizado su documentación de diferente forma, distinto orden y distintas prioridades.

Como consecuencia de esto, los técnicos deben tomarse el trabajo para analizar la instalación, que con frecuencia requiere desplazarse a otros sectores del establecimiento. El tiempo empleado en esta tarea se traduce en retrasos en los proyectos y perdidas económicas para la empresa que se verían reducidos o hasta eliminados si se dispone correctamente de la documentación.

<h2 id=Definicion> 3.Definición del sistema, Objetivos y Alcance </h2>

El sistema a desarrollar se centrará en facilitar la creación de documentación sobre las diferentes configuraciones usadas en cada uno de los sistemas electrónicos instalados por la empresa.  Para ello deberá proporcionar:
1. Plantillas para cada tipo de sistemas con los datos propios de cada producto componente, ademas de observaciones hechas por el técnico
2. Herramientas para agilizar el guardado de datos, de modo que sea práctico de registrar en medio de una instalación. Ej.: una herramienta que almacene impresiones de pantalla, otra que almacene las respuestas dadas por la shell a un determinado comando
3. Acceso a los datos al buscar por cliente, para conocer la configuración del mismo, y al buscar por producto por si se necesitan consultar para casos similares en el futuro
4. Se incluirá la posibilidad de exportar los documentos a formatos que puedan ser leidos desde programas básicos, como office, lectores PDF, etc.



<h2 id=Factibilidad> 4.Factibilidad</h2>

### Técnica

El sistema usará Java 8 y MySQL. Ambos Son Software gratuito por lo no aumentara los costos, sin embargo se deberá disponer de un pc que cumpla con con los requisitos de estos programas.

Requisitos de la computadora que ejecutará el sistema:
> RAM: 2Gb
> Disco Duro: 8Gb
> Procesador: x64 2 GHz o superior

Para el desarrollo en si se usara Eclipse para java y Lamp para la base de datos. Ambos Son Software gratuito.

### Económica

Costo estimado del Hardware:
> 1 Computadora: 220 Dolares 
Costo estimado del Desarrollo:
Se calcula que el proyecto requerirá de 4 meses para desarrollarse y 2 meses para implementarse.
> 1200 dolares

Beneficios estimados:
- Reducción de tiempos de ejecución
- Facilitará el que diferentes equipos (de técnicos) trabajen en etapas independientes de un mismo proyecto

### Operativa

Los principales usuarios de este sistema, aquellos encargados de cargar los datos, poseen altos conocimientos informáticos, que de por si ya son requeridos a la hora de configurar o reparar los equipos. 
Teniendo esto en cuenta, el software requerirá de estas capacidades para la mayor parte de sus herramientas, pero también es posible acceder a toda la información y cargar los datos meramente administrativos, con conocimientos básicos.
Por todo lo anterior se decidió no capacitar directamente a los empleados, pero si se elaborará un manual de usuario para el sistema

Ademas se incluirá la posibilidad de exportar los datos para facilitar la adaptación y asi disminuir la resistencia al cambio

### Conclusión

El proyecto es factible en todos sus aspectos, por lo que se encuentra listo para comenzar el desarrollo. 



<h2 id=Entidad> 5.Diagrama Entidad-Relación</h2>

<img src="http://localhost/multidoc/DER.png" alt="Diagrama ER">

<h2 id=Base> 6.Diseño de Base de Datos</h2>
<img src="http://localhost/multidoc/workbench.png" alt="Diagrama Casos de Uso">

<h2 id=Usos> 7.Diagrama de Casos de Usos</h2>
<img src="http://localhost/multidoc/casos%20de%20uso.png" alt="Diagrama Casos de Uso">

<h2 id=Menu> 8.Menú del Sistema</h2>

<h2 id=Codigo> 9.Desarrollo del Código</h2>

<h2 id=Conexion> 10.Conexión a base de datos</h2>

<h2 id=Consultas> 11.Consultas SQL</h2>

<h2 id=ABM> 12.ABM de una interfaz del sistema</h2>